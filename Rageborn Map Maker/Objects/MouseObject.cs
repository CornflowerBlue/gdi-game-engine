﻿using GDIGameEngine;
using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using GDIGameEngine.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rageborn_Map_Maker.Objects
{
    class MouseObject : GameObject
    {

        public MouseObject()
            :base(Sprite.FromTileSheetFile("Sheet.png", new Rectangle(32, 0, 32, 32)))
        {

        }

        public override void OnUpdate(float deltaTime)
        {
            Vector2 mouse = game.camera.ScreenToWorld(Mouse.MousePosition);

            SetPosition(new Vector2((FrmMain.TILESIZE / 2) + (int)(mouse.X / FrmMain.TILESIZE) * FrmMain.TILESIZE, (FrmMain.TILESIZE / 2) + (int)(mouse.Y / FrmMain.TILESIZE) * FrmMain.TILESIZE));

            base.OnUpdate(deltaTime);
        }
    }
}
