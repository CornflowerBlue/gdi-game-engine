﻿using GDIGameEngine;
using GDIGameEngine.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rageborn_Map_Maker.Objects
{
    class GridObject : GameObject
    {

        public GridObject()
            :base(Sprite.FromTileSheetFile("Sheet.png", new Rectangle(0, 0, 32, 32)))
        {

        }
    }
}
