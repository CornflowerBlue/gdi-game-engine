﻿using GDIGameEngine;
using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using GDIGameEngine.Input;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rageborn_Map_Maker
{
    public partial class FrmMain : Form
    {
        const int HEIGHT = 19;//608
        const int WIDTH = 34;//1088
        public const int TILESIZE = 32;

        public Game game;

        public FrmMain()
        {
            InitializeComponent();

            game = new Game(pictureBox1, pictureBox1.Width, pictureBox1.Height);
            pictureBox1.Resize += pictureBox1_Resize;
        }

        void pictureBox1_Resize(object sender, EventArgs e)
        {
            float width = WIDTH * TILESIZE;
            float height = HEIGHT * TILESIZE;

            float scaleW = 1f-(width - pictureBox1.Width) / width;
            float scaleH = 1f - (height - pictureBox1.Height) / height;

            game.camera.Scale = scaleW > scaleH ? scaleH : scaleW;

            game.RenderWidth = pictureBox1.Width;
            game.RenderHeight = pictureBox1.Height;
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            // Register as Pro
            game.RegisterPro(65861876);

            // Run the game
            game.Run();

            // Set settings
            game.DebugToggleKey = Keys.F12;             // Debug
            game.OnUpdate += game_OnUpdate;             // So we can see our FPS
            game.RefreshRate = 1000f / 24f;             // Limit FPS
            game.UseOcclusionCulling = false;

            Scene.ChunkSize = new System.Drawing.Size(WIDTH * TILESIZE, HEIGHT * TILESIZE);

            GenGrid();

            game.scene.AddObject(new Objects.MouseObject());

            // Fake a resize
            pictureBox1_Resize(pictureBox1, null);
        }

        private void GenGrid()
        {
            for (int y = 0; y < HEIGHT; y++)
            {
                for (int x = 0; x < WIDTH; x++)
                {
                    game.scene.AddObject(new Objects.GridObject(), new Vector2((FrmMain.TILESIZE / 2) + (x * TILESIZE), (FrmMain.TILESIZE / 2) + (y * TILESIZE)), 0);
                }
                game.scene.AddObject(new GameObject(Sprite.FromTileSheetFile("Sheet.png", new Rectangle(0, 0, 1, 32))), new Vector2((WIDTH * TILESIZE), (y * TILESIZE) + (FrmMain.TILESIZE / 2)), 0);
            }
        }

        void game_OnUpdate(float DeltaTime)
        {
            this.Text = string.Format("FPS: {0}, Scale: {1}, Size: {2}", game.GetCurrentFrameRate().ToString(), game.camera.Scale, this.Size);
        }

        private void toggleViewToolStripMenuItem_Click(object sender, EventArgs e) // Toggle Grid
        {
            game.scene.LayerSetting(0).ShouldDraw = !game.scene.LayerSetting(0).ShouldDraw;
            toggleViewToolStripMenuItem.Checked = game.scene.LayerSetting(0).ShouldDraw;
        }
    }
}
