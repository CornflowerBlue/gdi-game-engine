﻿using GDIGame.GameObjects;
using GDIGame.Scenes;
using GDIGameEngine;
using GDIGameEngine.Base;
using GDIGameEngine.Input;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Random = GDIGameEngine.GDIMath.Random;

namespace GDIGame
{
    public partial class FrmMain : Form
    {
        const int HEIGHT = 700;
        const int WIDTH = 608;

        public Game game;
        private Scene gameScene;

        public FrmMain()
        {
            InitializeComponent();

            this.Width = WIDTH + (SystemInformation.FrameBorderSize.Width * 4);
            this.Height = HEIGHT + (SystemInformation.FrameBorderSize.Height * 4) + SystemInformation.CaptionHeight;

            game = new Game(pictureBox1, WIDTH, HEIGHT);
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            // Run a Pro
            game.RegisterPro(60490150);

            // Run the game
            game.Run();

            // Set settings
            game.DebugToggleKey = Keys.F12;             // Debug
            game.BackgroundColor = Color.Black;         // Background color
            game.OnUpdate += game_OnUpdate;             // So we can see our FPS

            gameScene = game.scene = new BombermanScene(game);
        }

        void game_OnUpdate(float DeltaTime)
        {
            this.Text = "FPS: " + game.GetCurrentFrameRate().ToString() + "  Networking: " + game.Networking.GetMyID().ToString();
        }

        public void Render(Graphics graphics, Bitmap bitmap)
        {
            pictureBox1.Image = bitmap;
        }
    }
}