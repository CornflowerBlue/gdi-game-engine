﻿using GDIGame.GameObjects;
using GDIGame.GameObjects.Bomberman;
using GDIGameEngine;
using GDIGameEngine.Base;
using GDIGameEngine.Input;
using GDIGameEngine.Particles;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Random = GDIGameEngine.GDIMath.Random;
using Core;
using System.Diagnostics;
using GDIGameEngine.GUI;
using GDIGameEngine.GUI.Components;
using GDIGameEngine.GDIMath;

namespace GDIGame.Scenes
{
    class BombermanScene : Scene
    {
        public int seed;

        public const int tileSize = 32;

        public GameObject[,] mapObjectArray = new GameObject[19, 19];
        public int[,] mapArray = new int[19, 19];

        Dictionary<int, Player> players = new Dictionary<int, Player>();
        Player player;

        public BombermanScene(Game game)
            :base(game)
        {
            game.UseInbuiltCollisionDetection = false;
            game.camera.position = new Vector2(0, -92);

            seed = Random.Range2(1, int.MaxValue);
            setupGame();

            // GUI
            gui.AddComponent(new TextComponent("Test", new Vector2(10, 10), Brushes.Red));

            // Add networking events
            game.Networking.OnPlayerConnected += networking_OnPlayerConnected;
            game.Networking.OnDataReceived += networking_OnDataReceived;
            game.Networking.OnPlayerDisconnected += networking_OnPlayerDisonnected;
        }

        void setupGame()
        {
            mapArray = new int[19, 19];
            mapObjectArray = new GameObject[19, 19];

            foreach(GameObject go in GetAllObjectsIncLayerZero())
                go.Destroy();

            // Generation
            GenWorld(seed);

            // Add objects
            player = AddObject(new Player(this), new Vector2(48, 48), 6) as Player;
        }

        private void GenWorld(int seed)
        {
            Vector2 offset = new Vector2(16, 16);

            for (int i = 0; i < 19; i++)
            {
                mapArray[i, 0] = 1;
                mapArray[i, 18] = 1;

                mapArray[0, i] = 1;
                mapArray[18, i] = 1;
            }

            // Tiles
            for (int x = 1; x < 18; x++)
                for (int y = 1; y < 18; y++)
                {
                    Random.seed = x * seed * ((y + 1) + (x + seed)) * y;

                    if (x % 2 == 0 && y % 2 == 0)
                        mapArray[x, y] = 1;
                    else if (Random.Range(0f, 1.6f) <= 1f)
                        mapArray[x, y] = 2;
                }


            mapArray[1, 1] = 0;
            mapArray[2, 1] = 0;
            mapArray[1, 2] = 0;
            mapArray[1, 3] = 0;

            mapArray[17, 17] = 0;
            mapArray[17, 16] = 0;
            mapArray[17, 15] = 0;
            mapArray[16, 17] = 0;


            for (int x = 0; x < mapArray.GetLength(0); x++)
                for (int y = 0; y < mapArray.GetLength(1); y++)
                {
                    switch (mapArray[x, y])
                    {
                        case 0:
                            mapObjectArray[x, y] = AddObject(new GameObjects.Bomberman.BackgroundTile(), new Vector2(x * 32, y * 32) + offset, 0); break;
                        case 1:
                            mapObjectArray[x, y] = AddObject(new GameObjects.Bomberman.BlockedTile(), new Vector2(x * 32, y * 32) + offset, 0); break;
                        case 2:
                            AddObject(new GameObjects.Bomberman.BackgroundTile(), new Vector2(x * 32, y * 32) + offset, 0);
                            mapObjectArray[x, y] = AddObject(new GameObjects.Bomberman.Tile(), new Vector2(x * 32, y * 32) + offset, 1);
                            break;
                    }
                }
        }

        void networking_OnDataReceived(int SenderID, ushort Header, string[] Message)
        {
            if (Header == 0x00) // Map Gen
            {
                seed = int.Parse(Message[0]);
                setupGame();

                // Assume I am player 2, so put me in the second players start position
                player.SetPosition((new Vector2(17, 17) * 32) + 16);
            }
            else if (Header == 0x01) // Move
            {
                if (players.ContainsKey(SenderID))
                {
                    Vector2 newPos = new Vector2(int.Parse(Message[0]), int.Parse(Message[1]));
                    Player p = players[SenderID];

                    p.SetPosition((p.TargetPos * 32) + 16);
                    p.TargetPos = newPos;
                    p.isMoving = true;
                }
            }
            else if (Header == 0x02) // bomb
            {
                if (players.ContainsKey(SenderID))
                {
                    Vector2 newPos = new Vector2(int.Parse(Message[0]), int.Parse(Message[1]));
                    Player p = players[SenderID];

                    mapArray[(int)newPos.X, (int)newPos.Y] = 3;
                    AddObject(new Bomb(p, newPos));
                }
            }
        }

        void networking_OnPlayerDisonnected(int c)
        {
            if (players.ContainsKey(c))
            {
                Player p = players[c];
                p.Destroy();
            }
        }

        void networking_OnPlayerConnected(int c)
        {
            Player p = AddObject(new Player(this), 5) as Player;
            if (game.Networking.IsServer)
                p.SetPosition((new Vector2(17, 17) * 32) + 16);
            else
                p.SetPosition((new Vector2(1, 1) * 32) + 16);

            p.Phantom = true;
            players.Add(c, p);

            if(game.Networking.IsServer)
                game.Networking.SendPacket(c, 0x00, seed.ToString());
        }

        public override void UpdateScene(float deltaTime)
        {
            if (Keyboard.WasKeyDown(System.Windows.Forms.Keys.F5))
                game.Networking.Connect("localhost", 7778);

            if (Keyboard.WasKeyDown(System.Windows.Forms.Keys.F6))
                game.Networking.Host(7778, 2);

            if(Mouse.WasLMouseClicked)
            {
                Debug.Print(Mouse.MousePosition.ToString());
            }

            base.UpdateScene(deltaTime);
        }
    }
}
