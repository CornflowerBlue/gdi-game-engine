﻿using GDIGame.Scenes;
using GDIGameEngine;
using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDIGame.GameObjects.Bomberman
{
    class Tile : GameObject
    {
        public Tile()
            : base(Sprite.FromTileSheetFile("BomberManSheet.png", new Rectangle(32, 0, 32, 32)))
        {
        }

        public override void OnDestroy()
        {
            Vector2 myTileLoc = new Vector2(Position.X / 32, (Position.Y / 32));
            BombermanScene s = (BombermanScene)game.scene;
            s.mapArray[(int)myTileLoc.X, (int)myTileLoc.Y] = 0;

            base.OnDestroy();
        }
    }
}
