﻿using GDIGame.Scenes;
using GDIGameEngine;
using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDIGame.GameObjects.Bomberman
{
    class Explosion : GameObject
    {
        AnimatedSprite aS;

        public Explosion(Player parent, Vector2 tilePos)
            :base(new AnimatedSprite(Sprite.FromTileSheetFile("bomberManSheet.png", new Rectangle(0, 32, 32, 32)), 0.1f), (tilePos * 32))
        {
            aS = (AnimatedSprite)Sprite;
            aS.AddFrame("Main", Sprite.FromTileSheetFile("bomberManSheet.png", new Rectangle(0, 64, 32, 32)));
            aS.AddFrame("Main", Sprite.FromTileSheetFile("bomberManSheet.png", new Rectangle(0, 96, 32, 32)));
            aS.AddFrame("Main", Sprite.FromTileSheetFile("bomberManSheet.png", new Rectangle(0, 128, 32, 32)));
            aS.AddFrame("Main", Sprite.FromTileSheetFile("bomberManSheet.png", new Rectangle(0, 160, 32, 32)));

            aS.OnAnimationComplete += aS_OnAnimationComplete;
        }

        void aS_OnAnimationComplete(string animationName)
        {
            Destroy();
            aS.GotoFrame(2);
        }
    }
}
