﻿using GDIGame.Scenes;
using GDIGameEngine;
using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDIGame.GameObjects.Bomberman
{
    class Bomb : GameObject
    {
        AnimatedSprite aS;
        Player parent;

        public Bomb(Player parent, Vector2 tilePos)
        :base(new AnimatedSprite(Sprite.FromTileSheetFile("bomberManSheet.png", new Rectangle(192, 32, 32, 32)), 1f), (tilePos * 32) + 16)
        {
            this.parent = parent;

            aS = (AnimatedSprite)Sprite;
            aS.AddFrame("Main", Sprite.FromTileSheetFile("bomberManSheet.png", new Rectangle(160, 32, 32, 32)));
            aS.AddFrame("Main", Sprite.FromTileSheetFile("bomberManSheet.png", new Rectangle(128, 32, 32, 32)));

            aS.OnAnimationComplete += aS_OnAnimationComplete;
        }

        void aS_OnAnimationComplete(string animationName)
        {
            Destroy();
            aS.GotoFrame(2);
        }

        public override void OnDestroy()
        {
            Vector2 myTileLoc = new Vector2(Position.X / 32, (Position.Y / 32));

            BombermanScene s = (BombermanScene)game.scene;
            s.mapArray[(int)myTileLoc.X, (int)myTileLoc.Y] = 0;

            game.scene.AddObject(new Explosion(parent, myTileLoc), 1);


            // Up
            for (int i = 1; i <= parent.Range; i++)
            {
                int cU = PlaceExplosion((int)myTileLoc.X, (int)(myTileLoc.Y - i));
                if (cU >= 1)
                    game.scene.AddObject(new Explosion(parent, myTileLoc + (Vector2.Up * i)), 1);
                else break;

                if (cU == 2) break;
            }
            // Down
            for (int i = 1; i <= parent.Range; i++)
            {
                int cD = PlaceExplosion((int)myTileLoc.X, (int)(myTileLoc.Y + i));
                if (cD >= 1)
                    game.scene.AddObject(new Explosion(parent, myTileLoc + (Vector2.Down * i)), 1);
                else break;

                if (cD == 2) break;
            }
            // Left
            for (int i = 1; i <= parent.Range; i++)
            {
                int cL = PlaceExplosion((int)myTileLoc.X - i, (int)(myTileLoc.Y));
                if (cL >= 1)
                    game.scene.AddObject(new Explosion(parent, myTileLoc + (Vector2.Left * i)), 1);
                else break;

                if (cL == 2) break;
            }
            // Right
            for (int i = 1; i <= parent.Range; i++)
            {
                int cR = PlaceExplosion((int)myTileLoc.X + i, (int)(myTileLoc.Y));
                if (cR >= 1)
                    game.scene.AddObject(new Explosion(parent, myTileLoc + (Vector2.Right * i)), 1);
                else break;

                if (cR == 2) break;
            }


            base.OnDestroy();
        }

        private int PlaceExplosion(int x, int y)
        {
            if(x>=0 && x < 20 && y>=0 && y<20)
            {
                BombermanScene s = (BombermanScene)game.scene;

                if (s.mapArray[x, y] == 2)
                {
                    s.mapObjectArray[x, y].Destroy();
                    return 2;// Destroy but stop
                }

                if (s.mapArray[x, y] == 0)
                    return 1;// Destroy
            }
            return 0; // Stop
        }
    }
}
