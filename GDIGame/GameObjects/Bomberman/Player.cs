﻿using GDIGame.Scenes;
using GDIGameEngine;
using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using GDIGameEngine.Input;
using GDIGameEngine.Sound;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDIGame.GameObjects.Bomberman
{
    class Player : GameObject
    {
        private BombermanScene scene;

        public bool Phantom { get; set; }

        public Vector2 TargetPos = new Vector2(-1, -1);

        public bool isMoving;
        private float speed = 150;

        public int Range = 2;

        private AnimatedSprite aS;

        public Player(BombermanScene mainGame)
            : base(new AnimatedSprite(Sprite.FromTileSheetFile("player.png", new Rectangle(80, 0, 20, 30)), 0.1f))
        {
            this.scene = mainGame;

            // Animation
            aS = Sprite as AnimatedSprite;
            aS.AddFrame("WalkLeft", Sprite.FromTileSheetFile("player.png", new Rectangle(0, 0, 20, 30)));
            aS.AddFrame("WalkLeft", Sprite.FromTileSheetFile("player.png", new Rectangle(20, 0, 20, 30)));
            aS.AddFrame("WalkLeft", Sprite.FromTileSheetFile("player.png", new Rectangle(40, 0, 20, 30)));
            aS.AddFrame("WalkLeft", Sprite.FromTileSheetFile("player.png", new Rectangle(20, 0, 20, 30)));

            aS.AddFrame("WalkDown", Sprite.FromTileSheetFile("player.png", new Rectangle(60, 0, 20, 30)));
            aS.AddFrame("WalkDown", Sprite.FromTileSheetFile("player.png", new Rectangle(80, 0, 20, 30)));
            aS.AddFrame("WalkDown", Sprite.FromTileSheetFile("player.png", new Rectangle(100, 0, 20, 30)));
            aS.AddFrame("WalkDown", Sprite.FromTileSheetFile("player.png", new Rectangle(80, 0, 20, 30)));

            aS.AddFrame("WalkUp", Sprite.FromTileSheetFile("player.png", new Rectangle(120, 0, 20, 30)));
            aS.AddFrame("WalkUp", Sprite.FromTileSheetFile("player.png", new Rectangle(140, 0, 20, 30)));
            aS.AddFrame("WalkUp", Sprite.FromTileSheetFile("player.png", new Rectangle(160, 0, 20, 30)));
            aS.AddFrame("WalkUp", Sprite.FromTileSheetFile("player.png", new Rectangle(140, 0, 20, 30)));

            aS.AddFrame("WalkRight", Sprite.FromTileSheetFile("player.png", new Rectangle(180, 0, 20, 30)));
            aS.AddFrame("WalkRight", Sprite.FromTileSheetFile("player.png", new Rectangle(200, 0, 20, 30)));
            aS.AddFrame("WalkRight", Sprite.FromTileSheetFile("player.png", new Rectangle(220, 0, 20, 30)));
            aS.AddFrame("WalkRight", Sprite.FromTileSheetFile("player.png", new Rectangle(200, 0, 20, 30)));

            aS.CurrentAnimation = "";
        }

        public override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);
            Vector2 myTileLoc = new Vector2((int)(Position.X / 32), (int)(Position.Y / 32));

            if(!Phantom && Keyboard.WasKeyDown(Keys.Space))
            {
                if(scene.mapArray[(int)myTileLoc.X, (int)myTileLoc.Y] == 0)
                {
                    scene.mapArray[(int)myTileLoc.X, (int)myTileLoc.Y] = 3;
                    scene.AddObject(new Bomb(this, myTileLoc));

                    game.Networking.SendPacket(0x02, ((int)myTileLoc.X).ToString(), ((int)myTileLoc.Y).ToString());
                }
            }

            if(!isMoving && !Phantom)
            {
                if (Keyboard.IsKeyDown(System.Windows.Forms.Keys.W)) // Up
                {
                    if (scene.mapArray[(int)myTileLoc.X, (int)myTileLoc.Y - 1] == 0)
                    {
                        TargetPos = new Vector2(myTileLoc.X, myTileLoc.Y - 1);
                        isMoving = true;

                        aS.CurrentAnimation = "WalkUp";
                    }
                }
                else if (Keyboard.IsKeyDown(System.Windows.Forms.Keys.S)) // Down
                {
                    if (scene.mapArray[(int)myTileLoc.X, (int)myTileLoc.Y + 1] == 0)
                    {
                        TargetPos = new Vector2(myTileLoc.X, myTileLoc.Y + 1);
                        isMoving = true;

                        aS.CurrentAnimation = "WalkDown";
                    }
                }
                else if (Keyboard.IsKeyDown(System.Windows.Forms.Keys.A)) // Left
                {
                    if (scene.mapArray[(int)myTileLoc.X - 1, (int)myTileLoc.Y] == 0)
                    {
                        TargetPos = new Vector2(myTileLoc.X - 1, myTileLoc.Y);
                        isMoving = true;

                        aS.CurrentAnimation = "WalkLeft";
                    }
                }
                else if (Keyboard.IsKeyDown(System.Windows.Forms.Keys.D)) // Right
                {
                    if (scene.mapArray[(int)myTileLoc.X + 1, (int)myTileLoc.Y] == 0)
                    {
                        TargetPos = new Vector2(myTileLoc.X + 1, myTileLoc.Y);
                        isMoving = true;

                        aS.CurrentAnimation = "WalkRight";
                    }
                }

                if (isMoving)
                {
                    game.Networking.SendPacket(0x01, TargetPos.GetPoint().X.ToString(), TargetPos.GetPoint().Y.ToString());
                    aS.IsPlaying = true;
                }
                else
                {
                    aS.IsPlaying = false;
                    aS.GotoFrame(1);
                }
            }
            else if (isMoving)
            {
                Vector2 _target = TargetPos * BombermanScene.tileSize;
                _target += 16;

                if (Vector2.Distance(Position, _target) > 4)
                {
                    MoveTowards(_target, speed, deltaTime);
                }
                else
                {
                    SetPosition(_target);
                    isMoving = false;
                }
            }
        }
    }
}
