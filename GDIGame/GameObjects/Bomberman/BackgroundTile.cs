﻿using GDIGameEngine;
using GDIGameEngine.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDIGame.GameObjects.Bomberman
{
    class BackgroundTile : GameObject
    {
        public BackgroundTile()
            : base(Sprite.FromTileSheetFile("BomberManSheet.png", new Rectangle(64, 0, 32, 32)))
        {
            EnableCollisionDetection = false;
        }
    }
}
