﻿using GDIGameEngine;
using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZombieShooter
{
    public sealed class MapGenerator
    {
        const int MapSizeX = 100;
        const int MapSizeY = 100;
        const int TileSize = 32;

        public static void GenerateMap(Scene scene, int seed = 0)
        {

            for (int x = 0; x < MapSizeX; x++)
            {
                for (int y = 0; y < MapSizeY; y++)
                {
                    scene.AddObject(Tiles.GRASS, new Vector2(x * TileSize, y * TileSize) + new Vector2(TileSize / 2), 0);
                } 
            }
        }
    }
}
