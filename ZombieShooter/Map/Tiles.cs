﻿using GDIGameEngine;
using GDIGameEngine.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ZombieShooter
{
    public sealed class Tiles
    {
        public static GameObject GRASS
        {
            get
            {
                return new GameObject(Sprite.FromTileSheetFile("TileSheet.png", new Rectangle(0, 0, 32, 32)));
            }
        }
    }
}
