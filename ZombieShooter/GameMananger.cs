﻿using GDIGameEngine;
using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZombieShooter
{
    public sealed class GameMananger
    {
        public static GameMananger instance;

        private Game game;
        public Player.Player player;

        List<Player.Zombie> zombies = new List<Player.Zombie>();

        public GameMananger(Game game)
        {
            this.game = game;
            this.game.OnUpdate += game_OnUpdate;

            SetupGame();
        }

        private void SetupGame()
        {
            MapGenerator.GenerateMap(game.scene);
            player = game.scene.AddObject<Player.Player>(new Player.Player(), 8);

            zombies.Add(game.scene.AddObject<Player.Zombie>(new Player.Zombie(1), new Vector2(100, 100)));
        }

        void game_OnUpdate(float DeltaTime)
        {
            
        }
    }
}
