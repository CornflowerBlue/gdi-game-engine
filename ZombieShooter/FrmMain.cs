﻿using GDIGameEngine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ZombieShooter
{
    public partial class FrmMain : Form
    {
        const int HEIGHT = 600;
        const int WIDTH = 800;

        public Game game;

        public FrmMain()
        {
            InitializeComponent();

            this.Width = WIDTH + (SystemInformation.FrameBorderSize.Width * 4);
            this.Height = HEIGHT + (SystemInformation.FrameBorderSize.Height * 4) + SystemInformation.CaptionHeight;

            game = new Game(pictureBox1, WIDTH, HEIGHT);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Run a Pro
            game.RegisterPro(60490150);

            // Run the game
            game.Run();

            // Set settings
            game.DebugToggleKey = Keys.F12;
            game.OnUpdate += game_OnUpdate;


            GameMananger.instance = new GameMananger(game);
        }

        void game_OnUpdate(float DeltaTime)
        {
            this.Text = game.GetCurrentFrameRate().ToString();
        }
    }
}
