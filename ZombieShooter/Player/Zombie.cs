﻿using GDIGameEngine;
using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using GDIGameEngine.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ZombieShooter.Player
{
    public class Zombie : GameObject
    {
        const string TileSheet = "ZombieSheet.png";

        public AnimatedSprite aS;
        public State currentState = State.Idle;

        public bool isPlayer = false;

        public enum State
        {
            Idle,
            Walking,
            Running,
            Attacking
        }

        float attackSpeed = 1.2f;
        float attackTime = 0f;

        public Zombie(int ID)
            : base(new AnimatedSprite(Sprite.FromTileSheetFile(TileSheet, new Rectangle(0, 0, 16, 16)), 0.1f))
        {
            aS = Sprite as AnimatedSprite;

            aS.AddFrame("Walk", Sprite.FromTileSheetFile(TileSheet, new Rectangle(16, 0, 16, 16)));
            aS.AddFrame("Walk", Sprite.FromTileSheetFile(TileSheet, new Rectangle(32, 0, 16, 16)));
            aS.AddFrame("Walk", Sprite.FromTileSheetFile(TileSheet, new Rectangle(48, 0, 16, 16)));
            aS.AddFrame("Walk", Sprite.FromTileSheetFile(TileSheet, new Rectangle(0, 0, 16, 16)));

            aS.AddFrame("Attack", Sprite.FromTileSheetFile(TileSheet, new Rectangle(0, 32, 16, 16)));
            aS.AddFrame("Attack", Sprite.FromTileSheetFile(TileSheet, new Rectangle(0, 0, 16, 16)));

            aS.OnAnimationComplete += aS_OnAnimationComplete;
            this.Resize(3);
            this.Hitbox.Inflate(-10, -10);
        }

        void aS_OnAnimationComplete(string animationName)
        {
            if (currentState == State.Attacking)
            {
                currentState = State.Idle;
                UpdateAnimation();
            }
        }

        public override void OnUpdate(float deltaTime)
        {
            if (Vector2.Distance(Position, GameMananger.instance.player.Position) < 180)
            {
                if (Vector2.Distance(Position, GameMananger.instance.player.Position) > 32)
                {
                    MoveForward(80, deltaTime);
                    this.currentState = State.Walking;
                }
                else
                {
                    if (attackTime < 0)
                    {
                        attackTime = attackSpeed;
                        this.currentState = State.Attacking;
                    }
                    else
                    {
                        if (currentState == State.Walking) currentState = State.Idle;
                        attackTime -= deltaTime;
                    }
                }

                LookAt(GameMananger.instance.player);
            }
            else
            {
                this.currentState = State.Idle;
            }

            UpdateAnimation();
            base.OnUpdate(deltaTime);
        }

        public void UpdateAnimation()
        {
            switch (currentState)
            {
                case State.Walking: aS.CurrentAnimation = "Walk"; break;
                case State.Attacking: aS.CurrentAnimation = "Attack"; break;
                case State.Idle: aS.CurrentAnimation = ""; break;
            }
        }
    }
}
