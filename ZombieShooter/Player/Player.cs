﻿using GDIGameEngine;
using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using GDIGameEngine.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ZombieShooter.Player
{
    public class Player : Zombie
    {
        public Player()
            : base(0)
        {
            isPlayer = true;
        }
            
        public override void OnUpdate(float deltaTime)
        {
            LookAtMouse();
            game.camera.LookAt(this);

            SetState(deltaTime);
            UpdateAnimation();
        }

        private void SetState(float deltaTime)
        {
            if (Mouse.IsLeftButtonDown())
            {
                currentState = State.Attacking;
                return;
            }
            if (Keyboard.IsKeyDown(Keys.W) && (currentState == State.Idle || currentState == State.Walking || currentState == State.Walking))
            {
                MoveTowardsMouse(220, deltaTime);
                currentState = State.Walking;
            }
            else if (currentState != State.Attacking)
            {
                currentState = State.Idle;
            }
        }
    }
}
