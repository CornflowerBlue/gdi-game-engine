﻿using GDIGameEngine;
using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using GDIGameEngine.Input;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Gplus_Test_Area
{
    public partial class FrmMain : Form
    {
        public Game game;

        public List<Circle> Circles = new List<Circle>();
        public List<Rectangle> Rectangles = new List<Rectangle>();
        public Circle myCircle;

        public FrmMain()
        {
            InitializeComponent();

            this.Width = 800 + (SystemInformation.FrameBorderSize.Width * 4);
            this.Height = 600 + (SystemInformation.FrameBorderSize.Height * 4) + SystemInformation.CaptionHeight;

            game = new Game(pictureBox1, Width, Height);
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            // Run a Pro
            game.RegisterPro(90164098);

            // Run the game
            game.Run();

            // Set settings
            game.DebugToggleKey = Keys.F12;             // Debug
            game.OnUpdate += game_OnUpdate;             // So we can see our FPS
            game.OnDraw += game_OnDraw;

            Circles.Add(game.scene.AddObject(new Circle(Brushes.Green, 50), new Vector2(300, 300), 5));
            Circles.Add(game.scene.AddObject(new Circle(Brushes.Red, 100), new Vector2(300, 0), 5));
            Circles.Add(game.scene.AddObject(new Circle(Brushes.Yellow, 150), new Vector2(150, 150), 5));

            myCircle = game.scene.AddObject(new Circle(Brushes.Blue, 75), new Vector2(150, 150), 5);
            Rectangles.Add(new Rectangle(100,100,200,200));
        }

        void game_OnDraw(Graphics g)
        {
            foreach (var r in Rectangles)
                g.DrawRectangle(Pens.Blue, r);
        }

        bool CheckCircleColision(Circle c1, Circle c2)
        {
            return Vector2.Distance(c1.Position + (c1.size / 2), c2.Position + (c2.size / 2)) < ((c1.size / 2) + (c2.size / 2));
        }
        bool CheckRectangleColision(Circle circle, Rectangle rect)
        {
            int halfWidth = rect.Width / 2;
            int halfHeight = rect.Height / 2;

            float cx = Math.Abs((circle.Position.X + circle.r) - rect.X - halfWidth);
            float xDist = halfWidth + circle.r;
            if (cx > xDist)
                return false;
            float cy = Math.Abs((circle.Position.Y + circle.r) - rect.Y - halfHeight);
            float yDist = halfHeight + circle.r;
            if (cy > yDist)
                return false;
            if (cx <= halfWidth || cy <= halfHeight)
                return true;
            float xCornerDist = cx - halfWidth;
            float yCornerDist = cy - halfHeight;
            float xCornerDistSq = xCornerDist * xCornerDist;
            float yCornerDistSq = yCornerDist * yCornerDist;
            float maxCornerDistSq = circle.r * circle.r;
            return xCornerDistSq + yCornerDistSq <= maxCornerDistSq;
        }

        void game_OnUpdate(float DeltaTime)
        {
            this.Text = string.Format("FPS: {0}", game.GetCurrentFrameRate());

            myCircle.SetPosition(game.camera.ScreenToWorld(Mouse.MousePosition) - (myCircle.size / 2));

            myCircle.brush = Brushes.Blue;
            foreach (Circle c in Circles)
            {
                if (CheckCircleColision(myCircle, c))
                {
                    myCircle.brush = c.brush;
                    break;
                }
            }
            foreach (var g in Rectangles)
            {
                if (CheckRectangleColision(myCircle, g))
                {
                    myCircle.brush = Brushes.Pink;
                    break;
                }
            }
        }
    }

    public class Circle : GameObject
    {
        public int size = 100;
        public int r
        {
            get { return size / 2; }
        }
        public Brush brush;

        public Circle(Brush brush, int size = 100)
            : base(null)
        {
            this.size = size;
            this.brush = brush;
        }

        public override void OnRender(Graphics g)
        {
            g.FillEllipse(brush, DrawPosition.X, DrawPosition.Y, size, size);
        }
    }
    public class RotationObject : GameObject
    {
        public RotationObject()
            :base(Sprite.FromTileSheetFile("TileSheet.png", new Rectangle(0, 0, 32, 32)))
        {

        }
    }
}
