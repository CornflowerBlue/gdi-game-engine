﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio;
using NAudio.Wave;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace GDIGameEngine.Sound
{
    /// <summary>
    /// Credits to NAudio - http://naudio.codeplex.com/
    /// </summary>
    public sealed class SoundEngine
    {
        [DllImport("winmm.dll")]
        public static extern int waveOutGetVolume(IntPtr hwo, out uint dwVolume);

        [DllImport("winmm.dll")]
        public static extern int waveOutSetVolume(IntPtr hwo, uint dwVolume);

        private static float _volume = 0.3f;
        public static float volume
        {
            get { return _volume; }
            set
            {
                _volume = value;
                int NewVolume = (int)((ushort.MaxValue / 10) * value);
                // Set the same volume for both the left and the right channels
                uint NewVolumeAllChannels = (((uint)NewVolume & 0x0000ffff) | ((uint)NewVolume << 16));
                // Set the volume
                waveOutSetVolume(IntPtr.Zero, NewVolumeAllChannels);
            }
        }

        public static void Play(Sound sound)
        {
            if (volume == 0f) return;
            sound.Play(volume);
        }
    }

    public sealed class Sound
    {
        internal IWavePlayer waveOutDevice = null;
        AudioFileReader audioFileReader;
        DirectSoundOut audioOutput = new DirectSoundOut();

        FileType fileType;
        private enum FileType
        {
            mp3,
            wav
        }

        public Sound(string fileName)
        {
            fileName = "Assets\\" + fileName;

            Debug.Print(Path.GetExtension(fileName).ToLower());
            switch (Path.GetExtension(fileName).ToLower())
            {
                case ".wav":
                        WaveFileReader wfr = new WaveFileReader(fileName);
                        WaveChannel32 wc = new WaveChannel32(wfr) { PadWithZeroes = false };

                        audioOutput.Init(wc);

                        fileType = FileType.wav;
                        break;
                case ".mp3":
                        audioFileReader = new AudioFileReader(fileName);
                        waveOutDevice.Init(audioFileReader);

                        fileType = FileType.mp3;
                        break;
                default:
                        throw new Exception("File format not supported");
            }
            
        }

        internal void Play(float v)
        {
            switch(fileType)
            {
                case FileType.wav:
                    audioOutput.Play();
                    break;
                case FileType.mp3:
                    waveOutDevice.Play();
                break;
            }
        }
    }
}
