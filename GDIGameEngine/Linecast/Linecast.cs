﻿using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GDIGameEngine.Cast
{
    /// <summary>
    /// This uses the inbult colision, if you are not using that, this will not work
    /// </summary>
    public static class Linecast
    {
        internal static Game game;

        public static bool CreateCast(GameObject Source, GameObject Destination, float Range = 0f)// TODO: MaxRange
        {
            Vector2 checkPosition = Source.Position;
            Vector2 angle = Helper.Helper.GetAngleVBetweenObjects(Source, Destination);

            GameObject hit = Source;
            while (hit != Destination)
            {
                hit = game.scene.isBlocked(null, new Rectangle((int)checkPosition.X, (int)checkPosition.Y, 2, 2), angle);

                checkPosition += angle;
                
                if(hit != null && hit != Source)
                {
                    return false;
                }
            }

            return true;
        }

        public static GameObject CreateCast(GameObject Source, Vector2 Angle, float Range)
        {
            Vector2 checkPosition = Source.Position;

            GameObject hit = Source;
            while (Source.Position.Distance(checkPosition) <= Range)
            {
                hit = game.scene.isBlocked(null, new Rectangle((int)checkPosition.X, (int)checkPosition.Y, 2, 2), Angle);

                checkPosition += Angle;

                if (hit != null && hit != Source)
                {
                    return hit;
                }
            }

            return null;
        }
    }
}
