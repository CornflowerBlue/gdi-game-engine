﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDIGameEngine.Input
{
    public class Keyboard
    {
        [DllImport("User32.dll")]
        private static extern short GetAsyncKeyState(System.Int32 vKey);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        static List<int> oldState = new List<int>();

        public static void Update()
        {
            List<int> removeList = new List<int>();

            foreach (int key in oldState)
            {
                if (!IsKeyDown(key))
                {
                    removeList.Add(key);
                }
            }

            foreach (int rKey in removeList)
                oldState.Remove(rKey);
        }

        public static bool WasKeyDown(Keys vKey)
        {
            if (oldState.Contains((int)vKey))
            {
                return false;
            }
            else
            {
                if (IsKeyDown(vKey))
                {
                    oldState.Add((int)vKey);
                    return true;
                }
            }

            return false;
        }

        public static bool WasKeyUp(Keys vKey)
        {
            if (oldState.Contains((int)vKey))
            {
                if (!IsKeyDown(vKey))
                {
                    oldState.Remove((int)vKey);
                    return true;
                }
            }

            return false;
        }

        public static bool IsKeyDown(Keys vKey)
        {
            if (GetForegroundWindow() != Process.GetCurrentProcess().MainWindowHandle) return false;
            return 0 != (GetAsyncKeyState((int)vKey) & 0x8000);
        }
        public static bool IsKeyDown(int key)
        {
            if (GetForegroundWindow() != Process.GetCurrentProcess().MainWindowHandle) return false;
            return 0 != (GetAsyncKeyState(key) & 0x8000);
        }
    }
}
