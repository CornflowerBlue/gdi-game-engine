﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;
using GDIGameEngine.GDIMath;

namespace GDIGameEngine.Input
{
    public delegate void MouseMoveEvent(Vector2 Location);
    public class Mouse
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetCursorPos(out Point lpPoint);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        private const int KEY_PRESSED = 0x8000;

        private static bool _WasLMouseClicked;
        public static bool WasLMouseClicked
        { get; internal set; }

        private static bool _WasMMouseClicked;
        public static bool WasMMouseClicked
        { get; internal set; }

        private static bool _WasRMouseClicked;
        public static bool WasRMouseClicked
        { get; internal set; }

        internal static Game game;
        public static event MouseMoveEvent OnMouseMoved;

        static bool LeftDown = false;

        internal static void SetupEvents(PictureBox box)
        {
            box.MouseDown += box_MouseDown;
            box.MouseUp += box_MouseUp;
        }

        static void box_MouseUp(object sender, MouseEventArgs e)
        {
            switch(e.Button)
            {
                case MouseButtons.Left: LeftDown = false; break;
            }
        }

        static void box_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left: LeftDown = true; break;
            }
        }

        enum VirtualKeyStates : int
        {
            VK_LBUTTON = 0x01,
            VK_RBUTTON = 0x02,
            VK_MBUTTON = 0x04,
            VK_XBUTTON1 = 0x05,
            VK_XBUTTON2 = 0x06
        }

        public static bool IsLeftButtonDown()
        {
            if (GetForegroundWindow() != Process.GetCurrentProcess().MainWindowHandle) return false;

            Rectangle MouseRect = new Rectangle(GetCursorPos().GetPoint(), new Size(1, 1));
            
            if (game.scene.gui.InterceptsWithAnyComponent(MouseRect) != null)
                return false;

            return LeftDown;
        }
        public static bool IsMiddleButtonDown()
        {
            if (GetForegroundWindow() != Process.GetCurrentProcess().MainWindowHandle) return false;

            Rectangle MouseRect = new Rectangle(GetCursorPos().GetPoint(), new Size(1, 1));

            if (game.scene.gui.InterceptsWithAnyComponent(MouseRect) != null)
                return false;

            return LeftDown;
        }
        public static bool IsRightButtonDown()
        {
            if (GetForegroundWindow() != Process.GetCurrentProcess().MainWindowHandle) return false;

            Rectangle MouseRect = new Rectangle(GetCursorPos().GetPoint(), new Size(1, 1));

            if (game.scene.gui.InterceptsWithAnyComponent(MouseRect) != null)
                return false;

            return LeftDown;
        }

        // GUI
        public static bool GUI_IsLeftButtonDown()
        {
            if (GetForegroundWindow() != Process.GetCurrentProcess().MainWindowHandle) return false;

            return LeftDown;
        }
        public static bool GUI_IsMiddleButtonDown()
        {
            if (GetForegroundWindow() != Process.GetCurrentProcess().MainWindowHandle) return false;
            return LeftDown;
        }
        public static bool GUI_IsRightButtonDown()
        {
            if (GetForegroundWindow() != Process.GetCurrentProcess().MainWindowHandle) return false;
            return LeftDown;
        }

        private static Vector2 GetCursorPos()
        {
            Point p;
            if (GetCursorPos(out p))
            {
                game.renderWindow.Invoke(new Action(() => { p = game.renderWindow.PointToClient(p); }));
                return new Vector2((int)p.X, (int)p.Y);
            }
            return Vector2.Empty;
        }

        static Vector2 oldMousePos = Vector2.Empty;
        public static Vector2 MousePosition = Vector2.Empty;
        internal static void updateBefore()
        {
            if (GUI_IsLeftButtonDown() && !_WasLMouseClicked)
            {
                WasLMouseClicked = true;
                _WasLMouseClicked = true;
            }

            if (GUI_IsRightButtonDown() && !_WasRMouseClicked)
            {
                WasRMouseClicked = true;
                _WasRMouseClicked = true;
            }

            if (GUI_IsMiddleButtonDown() && !_WasMMouseClicked)
            {
                WasMMouseClicked = true;
                _WasMMouseClicked = true;
            }

            // GUI
            Rectangle MouseRect = new Rectangle(GetCursorPos().GetPoint(), new Size(1, 1));
            GUI.GUIComponent com;
            if (WasLMouseClicked && (com = game.scene.gui.InterceptsWithAnyComponent(MouseRect)) != null)
                com.OnLeftMouseClick();

        }
        internal static void updateAfter()
        {
            MousePosition = GetCursorPos();
            if (oldMousePos != MousePosition)
            {
                if (OnMouseMoved != null) OnMouseMoved.Invoke(MousePosition);
            }

            if (!GUI_IsLeftButtonDown()) _WasLMouseClicked = false;
            WasLMouseClicked = false;

            if (!GUI_IsRightButtonDown()) _WasRMouseClicked = false;
            WasRMouseClicked = false;

            if (!GUI_IsMiddleButtonDown()) _WasMMouseClicked = false;
            WasMMouseClicked = false;
        }
    }
}
