﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using GDIGameEngine.Helper;

namespace GDIGameEngine.Base
{
    public class Sprite : IDisposable
    {
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern IntPtr CreateCompatibleDC(IntPtr hdc);
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern bool DeleteDC(IntPtr hdc);
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern bool DeleteObject(IntPtr hObject);

        internal Bitmap Base_image;
        private Bitmap _Image;
        public Bitmap Image
        {
            get { return _Image; }
            set 
            {
                if (value == _Image) return;

                _Image = value;

                ContainsAlpha = Helper.Helper.IsAlphaBitmap(value);
                if (ContainsAlpha) return;

                // Color Mask
                using (Graphics grSrc = Graphics.FromImage(value))
                {
                    DeleteDC(pSource);
                    DeleteObject(pSource);

                    IntPtr pTarget = grSrc.GetHdc();
                    pSource = CreateCompatibleDC(pTarget);
                    IntPtr pOrig = SelectObject(pSource, value.GetHbitmap());
                    grSrc.ReleaseHdc(pTarget);

                }
            }
        }
        internal IntPtr pSource = IntPtr.Zero;

        internal bool ContainsAlpha = false;

        // Effects
        internal int rotation;
        internal float size;

        private static Dictionary<string, Sprite> Cache = new Dictionary<string, Sprite>();
        private static Dictionary<string, List<CropedImage>> CroppedCache = new Dictionary<string, List<CropedImage>>();
        private static Dictionary<string, Sprite> Base64Cache = new Dictionary<string, Sprite>();

        public Sprite(Bitmap image)
        {
            SetNewImage(image);
        }
        public Sprite(Image image)
        {
            this.Image = image as Bitmap;
            this.Base_image = image as Bitmap;
        }

        internal void Rotate(float Rotation, bool forced = false)
        {
            if ((int)Rotation != rotation || forced)
            {
                Helper.Helper.RotateImage(ref _Image, Base_image, Rotation);
                rotation = (int)Rotation;
            }
        }
        internal void Tint(Color tint)
        {
            Image = Helper.Helper.TintBitmap(Image, tint);
        }

        internal virtual void _Resize(float size)
        {
            this.size = size;
            SetNewImage(Helper.Helper.ResizeBitmap(Image, (int)(Image.Width * size), (int)(Image.Height * size)));
        }

        private void SetNewImage(Bitmap b)
        {
            Base_image = b;
            Image = b;
        }

        public void Dispose()
        {
            _Image.Dispose();
            Base_image.Dispose();
        }

        internal virtual void Update(float DeltaTime) { }

        // From File
        public static Sprite FromTileSheetFile(string ImagePath, Rectangle rect)
        {
            ImagePath = "Assets\\" + ImagePath;

            if (CroppedCache.ContainsKey(ImagePath))
            {
                foreach (CropedImage ci in CroppedCache[ImagePath])
                {
                    if (ci.rect == rect)
                        return ci.sprite;
                }
            }

            if (File.Exists(ImagePath))
            {
                Sprite rtn = new Sprite(Helper.Helper.Crop(Bitmap.FromFile(ImagePath) as Bitmap, rect));
                if (!CroppedCache.ContainsKey(ImagePath))
                {
                    CroppedCache.Add(ImagePath, new List<CropedImage>());
                }

                CroppedCache[ImagePath].Add(new CropedImage(rtn, rect));

                return rtn;
            }
            else
            {
                throw new FileNotFoundException();
            }
        }
        public static Sprite FromFile(string ImagePath)
        {
            ImagePath = "Assets\\" + ImagePath;

            if (Cache.ContainsKey(ImagePath))
                return Cache[ImagePath];

            if (File.Exists(ImagePath))
            {
                Sprite rtn = new Sprite(Bitmap.FromFile(ImagePath));
                Cache.Add(ImagePath, rtn);

                return rtn;
            }
            else
            {
                throw null;
            }
        }
        public static Sprite FromBase64(string base64)
        {
            string md5 = Helper.Helper.CalculateMD5Hash(base64);
            if (Base64Cache.ContainsKey(md5))
                return Base64Cache[md5];

            Sprite rtn = new Sprite(Helper.Helper.Base64ToImage(base64));
            Base64Cache.Add(md5, rtn);

            return rtn;
        }

        public static implicit operator Image(Sprite s)
        {
            return s.Image as Image;
        }
        public static implicit operator Size(Sprite s)
        {
            return new Size(s.Image.Width, s.Image.Height);
        }
    }

    internal class CropedImage
    {
        public Sprite sprite;
        public Rectangle rect;

        public CropedImage(Sprite sprite, Rectangle rect)
        {
            this.sprite = sprite;
            this.rect = rect;
        }
    }
}
