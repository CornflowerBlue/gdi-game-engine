﻿using Animation;
using GDIGameEngine.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GDIGameEngine.Base
{
    public delegate void AnimationEvent(string animationName);
    public class AnimatedSprite : Sprite
    {
        //  events
        public event AnimationEvent OnAnimationComplete;

        public int CurrentFrame = 0;
        private string _CurrentAnimation = "";
        public string CurrentAnimation
        {
            get
            {
                return _CurrentAnimation;
            }
            set
            {
                if (_CurrentAnimation != value)
                {
                    _CurrentAnimation = value;
                    GotoFrame(0);
                    curTime = 0;
                }
            }
        }

        private bool _IsPlaying = true;
        public bool IsPlaying
        {
            get
            {
                return _IsPlaying;
            }
            set
            {
                _IsPlaying = value;
                if (!IsPlaying)
                    curTime = 0;
            }
        }

        internal Dictionary<string, List<Frame>> frames = new Dictionary<string, List<Frame>>();

        public float time { get; set; }
        private float curTime;

        public AnimatedSprite(Sprite s, float time , params Sprite[] frames)
            : base(s.Base_image) 
        {
            this.time = time;
            AddFrame("", s);
        }

        public void AddFrame(string AnimationName, Sprite s)
        {
            if (!frames.ContainsKey(AnimationName)) frames.Add(AnimationName, new List<Frame>());
            frames[AnimationName].Add(new Frame(s.Base_image));
        }

        public void NextFrame()
        {
            GotoFrame(CurrentFrame + 1);
        }
        public void PreviousFrame()
        {
            GotoFrame(CurrentFrame - 1);
        }
        public void GotoFrame(int frame)
        {
            CurrentFrame = frame;
            if (CurrentFrame >= frames[CurrentAnimation].Count)
            {
                CurrentFrame = 0;
                if (OnAnimationComplete != null) OnAnimationComplete.Invoke(CurrentAnimation);
            }
            if (CurrentFrame < 0) CurrentFrame = frames[CurrentAnimation].Count;

            // Set frame
            Base_image = frames[CurrentAnimation][CurrentFrame].b;
            Image = Base_image;

            // Re-apply affects
            rotation = 0;
        }

        internal override void Update(float DeltaTime)
        {
            if(IsPlaying)
                curTime += DeltaTime;

            if (curTime >= time)
            {
                curTime = 0;
                NextFrame();
            }

            base.Update(DeltaTime);
        }
        internal override void _Resize(float size)
        {
            // Resize all frames
            foreach(var f in frames)
            {
                foreach(Frame ff in f.Value)
                    ff.b = Helper.Helper.ResizeBitmap(ff.b, (int)(Image.Width * size), (int)(Image.Height * size));
            }
        }
    }

}
namespace Animation
{
    class Frame
    {
        public Bitmap b;
        public Frame(Bitmap b)
        {
            this.b = b;
        }
    }
}
