﻿using GDIGameEngine.Base;
using GDIGameEngine.Input;
using GDIGameEngine.Networking;
using GDIGameEngine.Particles;
using GDIGameEngine.Cast;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using GDIGameEngine.Helper;
using GDIGameEngine.GUI;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
using System.Net;

namespace GDIGameEngine
{
    public delegate void Update(float DeltaTime);
    public delegate void Draw(Graphics g);

    /// <summary>
    /// GDI Game Engine
    /// http://gplus.rageborn.net/
    /// </summary>
    public class Game
    {
        const int UPDATE_SLEEP = 2;
        // Singleton
        static bool IsRunning;

        private bool IsPro = false;

        // Events
        public event Update OnUpdate;
        public event Draw OnDraw;

        internal PictureBox renderWindow;
        public int RenderWidth { get; set; }
        public int RenderHeight { get; set; }

        private SplashScreen _SplashScreen;

        // Game
        public Scene scene { get; set; }
        private TimeSpan oldTime;
        private Camera _camera;
        public Camera camera
        {
            get { return _camera; }
            set { if (value != null) _camera = value; }
        }
        public bool UseInbuiltCollisionDetection = false;

        // Drawing
        public Color BackgroundColor = Color.CornflowerBlue;
        public bool UseOcclusionCulling = true;
        public bool UseOcclusionCullingUpdating = false;
        public Quality quality = Quality.Highest;

        public enum Quality
        {
            HighestAA,
            Highest,
            Lowest
        }

        // Settings
        public Keys DebugToggleKey;
        internal int DrawDebugInfo = 0;
        public bool VSync = true;

        // Networking
        public NetworkingHandler Networking;

        // FPS/Timings
        public float TotalTime { get; internal set; }
        public float DeltaTime;
        public float RefreshRate = 1000f / 60f;

        // Threads
        private Thread gameUpdateLoop;
        private bool isRunning = false;

        public Game(PictureBox RenderPictureBox, int width, int height)
        {
            //Singleton
            if (IsRunning)
            {
                throw new Exception("A game instance is already running.");
            }

            IsRunning = true;

            this.renderWindow = RenderPictureBox;
            this.scene = new Scene(this);
            this.camera = new Camera(this);
            this.RenderWidth = width;
            this.RenderHeight = height;

            // Set up Static objects
            Mouse.game = this;
            Mouse.SetupEvents(RenderPictureBox);

            Linecast.game = this;

            RenderPictureBox.Disposed += RenderPictureBox_Disposed;
            RenderPictureBox.Paint += RenderPictureBox_Paint;

            Sound.SoundEngine.volume = 0.2f;

            InitNetworking();
        }
        ~Game()
        {
            Stop();
        }

        public void Run()
        {
            if(!IsPro)
                _SplashScreen = new SplashScreen(this);
            isRunning = true;
            SetupThreads();
        }

        public void Stop()
        {
            isRunning = false;

            DisposeThreads();
        }

        public bool RegisterPro(int AppID)
        {
            return IsPro = true;

            using (WebClient wc = new WebClient())
            {
                string FileMD5 = Helper.Helper.CalculateMD5Hash(System.IO.File.ReadAllText(Application.ExecutablePath));
                string response = wc.DownloadString(string.Format("http://gplus.rageborn.net/auth/auth.php?AppID={0}&MD5={1}", AppID, FileMD5));
                int code = int.Parse(response.Substring(0, response.IndexOf("#")));

                if (code == 1)
                    IsPro = true;
            }

            return IsPro;
        }

        private void InitNetworking()
        {
            Networking = new NetworkingHandler(this);
        }

        private void RenderPictureBox_Disposed(object sender, EventArgs e)
        {
            Stop();
        }
        
        private void SetupThreads()
        {
            gameUpdateLoop = new Thread(new ThreadStart(GameLoop));

            gameUpdateLoop.Start();
        }
        private void DisposeThreads()
        {
            if(gameUpdateLoop != null)
                gameUpdateLoop.Abort();
        }

        private void RenderPictureBox_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                Render(e.Graphics);

                if (OnDraw != null) OnDraw.Invoke(e.Graphics);
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
            }
        }

        private void Render(Graphics g)
        {
            switch (quality)
            {
                case Quality.HighestAA: g.ToHighQuality(); g.InterpolationMode = InterpolationMode.HighQualityBicubic; break;
                case Quality.Highest: g.ToHighQuality(); break;
                case Quality.Lowest: g.ToLowQuality(); break;
            }

            // Draw
            if (_SplashScreen != null)
                g.Clear(SplashScreen.ClearColor);
            else
                g.Clear(BackgroundColor);

            // Draw objects
            if (_SplashScreen != null) _SplashScreen.RenderScene(g, (int)(RenderWidth * camera.Scale), (int)(RenderHeight * camera.Scale));
            else scene.RenderScene(g, (int)(RenderWidth), (int)(RenderHeight));

            // Draw FPS
            DrawDebugInfomation(g);
        }

        private void DrawDebugInfomation(Graphics g)
        {
            if (DrawDebugInfo > 0)
            {
                if (DrawDebugInfo == 2)
                {
                    foreach (GameObject go in (scene.UseBackgroundLayerCollision ? scene.GetAllObjectsIncLayerZero() : scene.GetAllObjects()))
                    {
                        if (UseInbuiltCollisionDetection)
                        {
                            if (go == null) continue;
                            if (!go.Enabled || !go.EnableCollisionDetection) continue;
                            if (camera.IsInCameraView(go))
                                g.FillRectangle(new SolidBrush(Color.FromArgb(60, 255, 0, 0)), go.Bounds - camera.position);
                        }
                    }
                }

                if (DrawDebugInfo == 1)
                {
                    string DebugString = GetDebugData();

                    g.DrawString(DebugString, new Font("Times New Roman", 12f), Brushes.Black, new PointF(1, 1));
                    g.DrawString(DebugString, new Font("Times New Roman", 12f), Brushes.Black, new PointF(-1, -1));
                    g.DrawString(DebugString, new Font("Times New Roman", 12f), Brushes.Black, new PointF(1, 0));
                    g.DrawString(DebugString, new Font("Times New Roman", 12f), Brushes.Black, new PointF(-1, 0));
                    g.DrawString(DebugString, new Font("Times New Roman", 12f), Brushes.Yellow, PointF.Empty);
                }

                FPS.DrawFPSGraph(g);
            }
        }

        private string GetDebugData()
        {
            string TimingString = "";
            foreach (KeyValuePair<string, TimeSpan> t in scene.Timings)
                TimingString += t.Key + " - " + t.Value.TotalMilliseconds.ToString() + "ms" + Environment.NewLine;

            return string.Format("FPS: {1} : {2}{0}GameObjects: {3}, Static: {4}, Rendered: {8}{0}Particles: {7}, Rendered: {9}{0}Camera: {5}  Mouse: {6}{0}Networking: {11}{0}{10}",
                        Environment.NewLine, FPS.frameRate + ":" + FPS.callCount,
                        DeltaTime, scene.GetTotalObjectCount(),
                        scene.GetTotalStaticObjectCount(), camera.position.ToString(),
                        camera.ScreenToWorld(Mouse.MousePosition), scene.particleSystem.particles.Count,
                        scene.RenderedObjects, scene.RenderedParticles,
                        TimingString,
                        "Client: " + Networking.IsConnected + "  -  Server: " + Networking.IsServer);
        }

        internal void tick()
        {
            try
            {
                renderWindow.Invalidate();

                // Update Delta
                CalcDeltaTime();
                TotalTime += DeltaTime;

                // mouse
                Mouse.updateBefore();

                // Splash Screen Stuff
                if (_SplashScreen != null && TotalTime >= SplashScreen.Time)
                {
                    camera.position = _SplashScreen.startCamLoc;
                    _SplashScreen = null;
                }

                // Update objects
                if (_SplashScreen != null) _SplashScreen.UpdateScene(DeltaTime);
                else scene.UpdateScene(DeltaTime);

                camera.Update(DeltaTime);
                if (OnUpdate != null) OnUpdate.Invoke(DeltaTime);

                // Reset the Keyboard/Mouse
                Keyboard.Update();
                Mouse.updateAfter();

                // Update networking
                Networking.HandlePackets();

                // Debug
                if (Keyboard.WasKeyDown(Keys.F1))
                {
                    Debug.Print(GetDebugData());
                    Debug.Print("Particles: Count: {0}", scene.particleSystem.particles.Count);
                    foreach (GameObject go in scene.GetAllObjects())
                    {
                        Debug.Print("Object [{5}]: {0}, Location: {1}, Bounds: {2} - Hitbox: {6}, Rotation: {3} : {4}", go.GetType().Name, go.Position, go.Bounds, go.Rotation, go.Rotation * (System.Math.PI / 180), go.ObjectID, go.Hitbox);
                    }
                    foreach (GUIComponent c in scene.gui.GUIComponents)
                    {
                        Debug.Print("{0}GUI: {1}, Location: {2}, Bounds: {3}", (c.IsEnabled ? "*" : ""), c.GetType().Name, c.location, c.Bounds);
                    }
                }

                if (Keyboard.WasKeyDown(DebugToggleKey))
                {
                    DrawDebugInfo++;
                    if (DrawDebugInfo == 4) DrawDebugInfo = 0;
                }
            }
            catch (Exception e)
            {
                Debug.Print(e.ToString());
            }

            GC.Collect();
        }

        TimeSpan OldTime = DateTime.Now.TimeOfDay;
        private void CalcDeltaTime()
        {
            TimeSpan deltaTime = DateTime.Now.TimeOfDay - OldTime;
            DeltaTime = (float)deltaTime.TotalSeconds;

            OldTime = DateTime.Now.TimeOfDay;
        }

        private void GameLoop()
        {
            while (isRunning)
            {
                try
                {
                    oldTime = DateTime.Now.TimeOfDay;

                    // Invoke the draw
                    renderWindow.Invoke(new Action(tick));

                    // Add and remove tiles
                    scene.AddGameObjects();
                    scene.RemoveObjects();

                    // Hard coded scene
                    if (_SplashScreen != null)
                    {
                        _SplashScreen.AddGameObjects();
                        _SplashScreen.RemoveObjects();
                    }

                    // Lock FPS
                    TimeSpan TimeSinceLastDraw = DateTime.Now.TimeOfDay - oldTime;
                    if (VSync)
                    {
                        float sleep = (int)(RefreshRate - TimeSinceLastDraw.TotalMilliseconds);

                        if (sleep > 0)
                        {
                            Thread.Sleep((int)sleep);
                        }

                    }
                }
                catch (Exception e)
                {
                    Debug.Print(e.ToString());
                }
            }
            
        }

        public int GetCurrentFrameRate()
        {
            return FPS.frameRate;
        }

        public void HideMouse()
        {
            Cursor.Hide();
        }
        public void ShowMouse()
        {
            Cursor.Show();
        }
    }
}
