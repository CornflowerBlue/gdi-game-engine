﻿using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDIGameEngine.Base
{
    internal sealed class FPS
    {
        private static Double ptlu;
        internal static int callCount;

        public static int frameRate = 60;

        static List<int> fps = new List<int>();

        public static void Tick()
        {
            callCount += 1;

            if ((Environment.TickCount - ptlu) >= 1000)
            {
                frameRate = callCount;
                callCount = 0;
                ptlu = Environment.TickCount;

                fps.Add(frameRate);
                if (fps.Count >= 10) fps.RemoveAt(0);
            }
        }

        public static void DrawFPSGraph(Graphics g)
        {
            int h = GetHigestPoint();
            Vector2 offset = new Vector2(290, 4);

            Point _p1 = new Point((int)offset.X + 10, h - 60 + (int)offset.Y); 

            Point _p2 = new Point((int)offset.X + 80, _p1.Y);
            g.DrawLine(Pens.Blue, _p1, _p2);

            for (int x = 1; x < fps.Count-1; x++)
            {
                int pointY = fps[x];
                int point2Y = fps[x+1];

                Point p1 = new Point(x * 10, h-pointY);
                Point p2 = new Point((x + 1) * 10, h-point2Y);

                p1.X += (int)offset.X;
                p1.Y += (int)offset.Y;

                p2.X += (int)offset.X;
                p2.Y += (int)offset.Y;

                g.DrawLine(Pens.Red, p1, p2);
            }
        }

        private static int GetHigestPoint()
        {
            int highest = 0;
            foreach (int f in fps)
                if (f > highest) highest = f;

            return highest;
        }
    }
}
