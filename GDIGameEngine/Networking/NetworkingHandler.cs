﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core;
using Core.Packets;
using Core.Packets.ClientPackets;
using Core.Packets.ServerPackets;
using System.Diagnostics;

using Random = GDIGameEngine.GDIMath.Random;

namespace GDIGameEngine.Networking
{
    public delegate void DataReceivedEvent(int SenderID, ushort Header, string[] Message);
    public delegate void PlayerEvent(int ClientID);
    public delegate void Connect();
    /// <summary>
    /// Credits to Banksy 
    /// Header: 65,000 - 65,535 is reserved for engine purposes, this will not get called if the header is this between theses values.
    /// </summary>
    public sealed class NetworkingHandler
    {
        public event DataReceivedEvent OnDataReceived;
        public event PlayerEvent OnPlayerConnected;
        public event PlayerEvent OnPlayerDisconnected;
        public event Connect OnDisconnected;
        public event Connect OnConnected;

        internal List<tmpSPacket> serverPackets = new List<tmpSPacket>();
        internal List<tmpCPacket> clientPackets = new List<tmpCPacket>();

        private Game game;
        public bool IsServer;

        Server server;
        Client client;

        Dictionary<int, Client> assignedIDs = new Dictionary<int, Client>();
        List<int> connectedClients = new List<int>();

        public bool IsConnected { get; private set; }

        public NetworkingHandler(Game game)
        {
            this.game = game;
        }

        public bool Host(ushort port, ushort maxConection = 8) 
        {
            if (client == null)
            {
                IsServer = true;

                server = new Server(8192);
                server.AddTypesToSerializer(typeof(IPacket), new Type[]
                {
                    typeof(Initialize),
                    typeof(InitializeCommand),
                    typeof(DataPacket),
                    typeof(DisconnectPacket),
                    typeof(DirectDataPacket)
                });

                server.ServerState += server_ServerState;
                server.ClientRead += server_ClientRead;
                server.ClientState += server_ClientState;

                server.MaxConnections = maxConection;

                server.Listen(port);

                return true;
            }
            return false;
        }
        public bool Connect(string IP, ushort port)
        {
            if (server == null)
            {
                client = new Client(8192);

                client.AddTypesToSerializer(typeof(IPacket), new Type[]
                {
                    typeof(Initialize),
                    typeof(InitializeCommand),
                    typeof(DataPacket),
                    typeof(DisconnectPacket),
                    typeof(DirectDataPacket)
                });

                client.ClientState += client_ClientState;
                client.ClientRead += client_ClientRead;

                //And finally, connect.
                client.Connect(IP, port);
                return true;
            }
            return false;
        }

        void server_ServerState(Server s, bool listening)
        {
            Debug.Print("Listening = " + listening.ToString());

            if (!listening) if (OnDisconnected != null) OnDisconnected.Invoke();
        }

        void server_ClientState(Server s, Client c, bool connected)
        {
            if (connected)
            {
                if (!assignedIDs.ContainsValue(c))
                {
                    c.Value = new UserState();
                    c.Value.ID = AssignNewID();
                    assignedIDs[c.Value.ID] = c;

                    Debug.Print("Client[{0}] has Connected", c.Value.ID);

                    new InitializeCommand(c.Value.ID).Execute(c);
                }
            }
            else
            {
                // Player disconnected
                if (OnPlayerDisconnected != null) OnPlayerDisconnected.Invoke(c.Value.ID);
                // Tell the other clients
                foreach (Client nc in server.Clients)
                {
                    Debug.Print("Telling Client [{0}] that Client [{1}] disconnected", nc.Value.ID, c.Value.ID);
                    new DisconnectPacket(c.Value.ID).Execute(nc);
                }

                Debug.Print("Client[{0}] has Disonnected", c.Value.ID);
                assignedIDs.Remove(c.Value.ID);
            }
        }

        private int AssignNewID()
        {
            int checkID = Random.Range2(10, int.MaxValue / 2);

            while(assignedIDs.ContainsKey(checkID))
                checkID = Random.Range2(10, int.MaxValue / 2);

            assignedIDs.Add(checkID, null);

            return checkID;
        }

        void server_ClientRead(Server s, Client c, IPacket _packet)
        {
            serverPackets.Add(new tmpSPacket(s, c, _packet));
        }
        void _server_ClientRead(Server s, Client c, IPacket packet)
        {
            Type type = packet.GetType();

            if (type == typeof(Initialize))
            {
                Initialize i = (Initialize)packet;
                if (connectedClients.Contains(c.Value.ID))
                    return;

                connectedClients.Add(c.Value.ID);
                Debug.Print("Got initialize Response");
                if (OnPlayerConnected != null) 
                    OnPlayerConnected.Invoke(c.Value.ID);

                foreach (Client cl in server.Clients)
                {
                    if (cl.Value.ID != c.Value.ID)
                    {
                        new Initialize("", cl.Value.ID).Execute(c);// Tell about the other players
                        new Initialize("", c.Value.ID).Execute(cl);
                    }
                }
                //Tell the new client about myself - the server
                new Initialize("", 0).Execute(c);
            }
            else if (type == typeof(DataPacket))
            {
                DataPacket dp = (DataPacket)packet;
                HandlePacket(dp);

                if(dp.Header >= 65000) // System
                {

                    return;
                }
                // Mirror to everybody else
                foreach (Client cl in server.Clients)
                    if(cl != c)
                        dp.Execute(cl);
            }
            else if (type == typeof(DirectDataPacket))
            {
                DirectDataPacket ddp = (DirectDataPacket)packet;
                ddp.Execute(assignedIDs[ddp.Receiver]);
            }
        }

        void client_ClientRead(Client s, IPacket packet)
        {
            clientPackets.Add(new tmpCPacket(s, packet));
        }
        void _client_ClientRead(Client s, IPacket packet)
        {
            Type type = packet.GetType();

            if (type == typeof(Initialize))
            {
                Initialize i = (Initialize)packet;
                if (OnPlayerConnected != null) OnPlayerConnected.Invoke(i.Client);
            }
            else if (type == typeof(InitializeCommand))
            {
                InitializeCommand ic  = (InitializeCommand)packet;
                Debug.Print("Got initialize Command, my ID is: " + ic.ID);
                client.Value = new UserState();
                client.Value.ID = ic.ID;
                new Initialize("hello!", ic.ID).Execute(client);
            }
            else if (type == typeof(DataPacket))
            {
                DataPacket dp = (DataPacket)packet;

                if (dp.Header >= 65000) // System
                {

                    return;
                }
                HandlePacket(dp);
            }
            else if (type == typeof(DisconnectPacket))
            {
                DisconnectPacket dcp = (DisconnectPacket)packet;
                if (OnPlayerDisconnected != null) OnPlayerDisconnected.Invoke(dcp.ID);
            }
            else if (type == typeof(DirectDataPacket))
            {
                DirectDataPacket ddp = (DirectDataPacket)packet;
                HandlePacket(new DataPacket(ddp.SenderID, ddp.Header, ddp.Message));
            }
        }

        internal void HandlePackets()
        {
            foreach (tmpSPacket sp in serverPackets)
                _server_ClientRead(sp.server, sp.client, sp.packet);

            foreach (tmpCPacket sc in clientPackets)
                _client_ClientRead(sc.server, sc.packet);

            serverPackets.Clear();
            clientPackets.Clear();
        }

        void HandlePacket(DataPacket dp)
        {
            if (OnDataReceived != null) OnDataReceived.Invoke(dp.SenderID, dp.Header, dp.Message);
        }

        void client_ClientState(Client s, bool connected)
        {
            Debug.Print("{0}onnected", connected ? "C" : "Disc");

            IsConnected = connected;

            if (connected) if (OnConnected != null) OnConnected.Invoke();
            if (!connected) if (OnDisconnected != null) OnDisconnected.Invoke();
        }

        /// <summary>
        /// Credits to Banksy.
        /// Header: 65,000 - 65,535 is reserved for engine purposes, this will not get called if the header is this between theses values.
        /// </summary>
        public void SendPacket(ushort Header, params string[] Message)
        {
            if (IsServer)
            {
                foreach (Client c in server.Clients)
                    new DataPacket(0, Header, Message).Execute(c);
            }
            else if (IsConnected)
                new DataPacket(GetMyID(), Header, Message).Execute(client);
        }
        /// <summary>
        /// Credits to Banksy.
        /// Header: 65,000 - 65,535 is reserved for engine purposes, this will not get called if the header is this between theses values.
        /// </summary>
        public void SendPacket(int ClientID, ushort Header, params string[] Message)
        {
            if (IsServer)
            {
                if (!assignedIDs.ContainsKey(ClientID))
                {
                    throw new Exception("Error: Tried sending a packet to an unknown client.");
                }
                else
                    new DirectDataPacket(0, ClientID, Header, Message).Execute(assignedIDs[ClientID]);
            }
            else if (IsConnected)
                new DirectDataPacket(GetMyID(), ClientID, Header, Message).Execute(client);
        }

        public int GetMyID()
        {
            if (IsConnected && client.Value != null) 
                return client.Value.ID;

            return 0;
        }
    }

    internal class tmpSPacket
    {
        public Server server;
        public Client client;
        public IPacket packet;

        public tmpSPacket(Server server, Client client, IPacket packet)
        {
            this.server = server;
            this.client = client;
            this.packet = packet;
        }
    }
    internal class tmpCPacket
    {
        public Client server;
        public IPacket packet;

        public tmpCPacket(Client server, IPacket packet)
        {
            this.server = server;
            this.packet = packet;
        }
    }
}
