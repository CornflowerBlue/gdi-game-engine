﻿using ProtoBuf;
namespace Core.Packets.ClientPackets
{
    [ProtoContract]
    public class DataPacket : IPacket
    {
        [ProtoMember(1)]
        public ushort Header { get; set; }

        [ProtoMember(2)]
        public string[] Message { get; set; }

        [ProtoMember(3)]
        public int SenderID { get; set; }

        public DataPacket() { }
        public DataPacket(int SenderID, ushort Header, params string[] Message)
        {
            this.SenderID = SenderID;
            this.Header = Header;
            this.Message = Message;
        }

        public void Execute(Client client)
        {
            client.Send<DataPacket>(this);
        }
    }
}
