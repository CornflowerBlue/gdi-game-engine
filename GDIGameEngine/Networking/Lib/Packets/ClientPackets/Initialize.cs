﻿using ProtoBuf;
namespace Core.Packets.ClientPackets
{
    [ProtoContract]
    public class Initialize : IPacket
    {
        [ProtoMember(1)]
        public string Message { get; set; }

        [ProtoMember(2)]
        public int Client { get; set; }

        public Initialize() { }
        public Initialize(string message, int Client)
        {
            Message = message;
            this.Client = Client;
        }

        public void Execute(Client client)
        {
            client.Send<Initialize>(this);
        }
    }
}
