﻿using ProtoBuf;
namespace Core.Packets.ServerPackets
{
    [ProtoContract]
    public class DirectDataPacket : IPacket
    {
        [ProtoMember(1)]
        public ushort Header { get; set; }

        [ProtoMember(2)]
        public string[] Message { get; set; }

        [ProtoMember(3)]
        public int SenderID { get; set; }

        [ProtoMember(4)]
        public int Receiver { get; set; }


        public DirectDataPacket() { }
        public DirectDataPacket(int SenderID, int Receiver, ushort Header, params string[] Message)
        {
            this.SenderID = SenderID;
            this.Receiver = Receiver;
            this.Header = Header;
            this.Message = Message;
        }
        public void Execute(Client client)
        {
            client.Send<DirectDataPacket>(this);
        }
    }
}
