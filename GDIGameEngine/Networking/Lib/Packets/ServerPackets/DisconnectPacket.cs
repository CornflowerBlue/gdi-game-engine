﻿using ProtoBuf;
namespace Core.Packets.ServerPackets
{
    [ProtoContract]
    public class DisconnectPacket : IPacket
    {
        [ProtoMember(1)]
        public int ID;

        public DisconnectPacket() { }
        public DisconnectPacket(int ID)
        {
            this.ID = ID;
        }
        public void Execute(Client client)
        {
            client.Send<DisconnectPacket>(this);
        }
    }
}
