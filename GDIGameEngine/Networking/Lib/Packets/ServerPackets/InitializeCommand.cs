﻿using ProtoBuf;
namespace Core.Packets.ServerPackets
{
    [ProtoContract]
    public class InitializeCommand : IPacket
    {
        [ProtoMember(1)]
        public int ID;

        public InitializeCommand() { }
        public InitializeCommand(int ID) 
        {
            this.ID = ID;
        }
        public void Execute(Client client)
        {
            client.Send<InitializeCommand>(this);
        }
    }
}
