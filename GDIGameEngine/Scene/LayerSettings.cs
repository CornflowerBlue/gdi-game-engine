﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDIGameEngine.Settings
{
    public sealed class LayerSettings
    {
        public bool UseOcclusionCulling = true;
        public bool UseOcclusionCullingUpdating = false;

        public bool ShouldUpdate = true;
        public bool ShouldDraw = true;

        public Dictionary<int, bool> IgnoreCollisionLayer = new Dictionary<int, bool>
        {
            {0, false},
            {1, false},
            {2, false},
            {3, false},
            {4, false},
            {5, false},
            {6, false},
            {7, false},
            {8, false},
            {9, false},
            {10, false}
        };
    }
}
