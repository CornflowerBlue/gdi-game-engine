﻿using GDIGameEngine.GDIMath;
using GDIGameEngine.Particles;
using GDIGameEngine.Settings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GDIGameEngine.Base
{
    public class Scene
    {
        public ParticleSystem particleSystem { get; internal set; }
        public bool UseBackgroundLayerCollision = false;

        internal Dictionary<string, TimeSpan> Timings = new Dictionary<string, TimeSpan>()
        {
            {"Draw_Background", TimeSpan.Zero},
            {"Draw_Objects", TimeSpan.Zero},
            {"Draw_Particles", TimeSpan.Zero},
            {"Draw_GUI", TimeSpan.Zero},
            {"Update_Objects", TimeSpan.Zero},
            {"Update_Particles", TimeSpan.Zero},
            {"Update_GUI", TimeSpan.Zero}
        };

        private List<GameObject>[] objects;
        private List<TempObject> objectsToAdd;
        private List<TempObject> objectsToRemove;

        internal Dictionary<uint, GameObject> ObjectIDs = new Dictionary<uint, GameObject>();

        // Coliders
        public List<Rectangle> HiddenColiders = new List<Rectangle>();

        // Settings
        private List<LayerSettings> LayerSettings = new List<LayerSettings>(11);
        public LayerSettings LayerSetting(int layerID) { return LayerSettings[layerID]; }

        // Object Chunks
        private Dictionary<Vector2, GameObject> ObjectChunks = new Dictionary<Vector2, GameObject>();

        // Drawing
        // Background
        private bool backgroundChanged = true;
        private Dictionary<Vector2, ChunkGraphic> backgroundImages = new Dictionary<Vector2, ChunkGraphic>();

        /// <summary>
        /// This is used for rendering the Background layer
        /// The background layer renders in chunks, as background objects should only get rendered once.
        /// (512, 512) Default.
        /// </summary>
        public static Size ChunkSize = new Size(512, 512);

        // GUI
        public readonly GUI.GUI gui;

        public Game game;

        public float deltaTime { get { return game.DeltaTime; } }

        public int ParticleLayer = 7;

        internal int RenderedObjects = 0;
        internal int RenderedParticles = 0;

        public Scene(Game game)
        {
            this.game = game;
            this.gui = new GUI.GUI(this);

            particleSystem = new ParticleSystem(game);

            objects = new List<GameObject>[11];
            objectsToAdd = new List<TempObject>();
            objectsToRemove = new List<TempObject>();

            // Fill the arrays
            for (int i = 0; i < objects.Length; i++)
                objects[i] = new List<GameObject>();

            for (int i = 0; i < LayerSettings.Capacity; i++)
                LayerSettings.Add(new LayerSettings());
        }
        /// <summary>
        /// Layer [0] Only gets drawn once - Dosn't update.
        /// Layer [1] Only redraws when an object has changed, - Does update.
        /// Layer [2-10] Always draw - Always updates.
        /// </summary>
        /// <param name="go"></param>
        /// <param name="LayerID"></param>
        public T AddObject<T>(T gameObject, Vector2 pos, int LayerID = 5) where T : GameObject
        {
            gameObject.SetPosition(pos);
            return AddObject(gameObject, LayerID) as T;
        }
        /// <summary>
        /// Layer [0] Only gets drawn once - Dosn't update.
        /// Layer [1] Only redraws when an object has changed, - Does update.
        /// Layer [2-10] Always draw - Always updates.
        /// </summary>
        /// <param name="go"></param>
        /// <param name="LayerID"></param>
        public T AddObject<T>(T go, int LayerID = 5) where T : GameObject
        {
            if (LayerID < 0 || LayerID > 10) throw new Exception("Invalid Layer.");
            
            SetupGameObject(go, LayerID);

            objectsToAdd.Add(new TempObject(LayerID, go));

            return go;
        }
        public void SetupGameObject(GameObject go, int LayerID)
        {
            go.game = game;
            go.Layer = LayerID;
            if(LayerID > 0)
                AssignObjectID(go);
            go.OnAdded();
        }

        private uint AssignObjectID(GameObject go)
        {
            uint newID = 1;
            if(ObjectIDs.Count > 0)
                newID = ObjectIDs.Last().Key + 1;

            while (ObjectIDs.ContainsKey(newID))
                newID++;

            ObjectIDs.Add(newID, go);

            go.ObjectID = newID;
            return newID;
        }

        public void RemoveObject(GameObject go)
        {
            objectsToRemove.Add(new TempObject(0, go));
        }

        internal void AddGameObjects()
        {
            foreach (TempObject to in objectsToAdd)
            {
                objects[to.layerID].Add(to.gameObject);

                if (to.layerID == 0) backgroundChanged = true;
            }

            objectsToAdd.Clear();
        }
        internal void RemoveObjects()
        {
            foreach (TempObject to in objectsToRemove)
            {
                to.gameObject.OnRemove();
                for (int i = 0; i < objects.Length; i++)
                    objects[i].Remove(to.gameObject); // Remove the object from each layer (Just in case it is in multiple)
            }
        }

        public virtual void UpdateScene(float deltaTime)
        {
            Stopwatch sw1 = new Stopwatch(); sw1.Start();
            for (int i = 1; i < objects.Length; i++)
            {
                if (!LayerSetting(i).ShouldUpdate) continue;
                foreach (GameObject go in objects[i])
                {
                    if (go == null) continue;
                    if (game.UseOcclusionCullingUpdating && LayerSettings[i].UseOcclusionCullingUpdating && !game.camera.IsInCameraView(go))
                        continue;

                    go.Update(deltaTime);
                }
            }
            sw1.Stop();
            Timings["Update_Objects"] = sw1.Elapsed;

            sw1 = new Stopwatch(); sw1.Start();
            particleSystem.Update(deltaTime);
            sw1.Stop();
            Timings["Update_Particles"] = sw1.Elapsed;

            // Update GUI
            sw1 = new Stopwatch(); sw1.Start();
            gui.Update(deltaTime);
            sw1.Stop();
            Timings["Update_GUI"] = sw1.Elapsed;
        }

        public virtual void RenderScene(Graphics g, int width, int height)
        {
            FPS.Tick();
            RenderedObjects = 0;
            RenderedParticles = 0;

            RenderBackground(g, width, height);
            RenderObjectsAndParticles(g, width, height);
            RenderGUI(g, width, height);

            if (game.DrawDebugInfo >= 1)
                g.DrawRectangle(Pens.Red, new Rectangle(0, 0, width, height));
        }
        private void RenderObjectsAndParticles(Graphics g, int width, int height)
        {
            Stopwatch sw1 = new Stopwatch(); sw1.Start();
            Stopwatch sw2 = new Stopwatch();

            // Draw other objects
            for (int i = 1; i < objects.Length; i++)
            {
                if (!LayerSetting(i).ShouldDraw) continue;
                foreach (GameObject go in objects[i])
                {
                    if (go == null) continue;
                    if (game.UseOcclusionCulling && LayerSettings[i].UseOcclusionCulling && !game.camera.IsInCameraView(go) && go.Sprite != null)
                        continue;

                    RenderedObjects++;

                    go.Render(g);
                }

                if (ParticleLayer == i)
                {
                    sw2.Start();
                    RenderedParticles = particleSystem.Draw(g);
                    sw2.Stop();
                    Timings["Draw_Particles"] = sw2.Elapsed;
                }
            }
            sw1.Stop();
            Timings["Draw_Objects"] = sw1.Elapsed - sw2.Elapsed;
        }
        private void RenderBackground(Graphics g, int width, int height)
        {
            if (!LayerSetting(0).ShouldDraw) return;

            Stopwatch sw1 = new Stopwatch(); sw1.Start();
            // Render the background layer
            if (backgroundChanged)
            {
                // Resize the background image & Offset
                foreach (GameObject go in objects[0])
                {
                    if (go == null) continue;
                    Vector2 chunkLocation = new Vector2((int)(go.positionWithOrigin.X / ChunkSize.Width) * ChunkSize.Width, ((int)go.positionWithOrigin.Y / (int)ChunkSize.Height) * ChunkSize.Height);
                    if (go.positionWithOrigin.X < 0) chunkLocation.X -= ChunkSize.Width;
                    if (go.positionWithOrigin.Y < 0) chunkLocation.Y -= ChunkSize.Height;

                    DrawBackgroundTile(chunkLocation, go);
                }
                backgroundChanged = false;

                foreach (KeyValuePair<Vector2, ChunkGraphic> chunk in backgroundImages)
                    chunk.Value.Recalc();
            }

            foreach (KeyValuePair<Vector2, ChunkGraphic> bg in backgroundImages)
            {
                Vector2 pos = (bg.Key) - game.camera.position.GetPoint();

                if (game.UseOcclusionCulling && !game.camera.IsInCameraView(bg.Key, ChunkSize.Width, ChunkSize.Height))
                {
                    if (game.DrawDebugInfo == 2)
                        g.DrawRectangle(Pens.Red, new Rectangle((int)(pos.X * game.camera.Scale), (int)(pos.Y * game.camera.Scale), (int)(ChunkSize.Width * game.camera.Scale), (int)(ChunkSize.Height * game.camera.Scale)));
                    continue;
                }

                Rendering.Render.RenderBackgroundChunk(game, g, bg.Value, pos);


                if (game.DrawDebugInfo == 2)
                    g.DrawRectangle(Pens.Blue, new Rectangle((int)(pos.X * game.camera.Scale), (int)(pos.Y * game.camera.Scale), (int)(ChunkSize.Width * game.camera.Scale), (int)(ChunkSize.Height * game.camera.Scale)));
                RenderedObjects++; // Background is a object
            }
            sw1.Stop();
            Timings["Draw_Background"] = sw1.Elapsed;
        }
        private void RenderGUI(Graphics g, int width, int height)
        {
            // Render GUI
            Stopwatch sw1 = new Stopwatch(); sw1.Start();
            gui.Render(g);
            sw1.Stop();
            Timings["Draw_GUI"] = sw1.Elapsed;
        }

        internal void DrawBackgroundTile(Vector2 chunk, GameObject go)
        {
            if (!backgroundImages.ContainsKey(chunk))
            {
                Bitmap b = new Bitmap(ChunkSize.Width, ChunkSize.Height);
                Graphics _g = Graphics.FromImage(b);
                backgroundImages.Add(chunk, new ChunkGraphic(b, _g));

                _g.Clear(game.BackgroundColor);
            }

            Graphics g = backgroundImages[chunk].g;
            g.DrawImage(go.Sprite, go.positionWithOrigin - chunk);

            // If the object intercepts any other chunks, draw on that chunk too
            Rectangle bounds = go.Bounds - chunk + go.Origin;
            if (bounds.Right > ChunkSize.Width)// Overlaps right chunk
                DrawBackgroundTile(chunk + new Vector2(ChunkSize.Width, 0), go);

            if (bounds.Bottom > ChunkSize.Height)// Overlaps below chunk
                DrawBackgroundTile(chunk + new Vector2(0, ChunkSize.Height), go);
        }

        public int GetTotalObjectCount()
        {
            int count = 0;
            for (int i = 1; i < objects.Length; i++)
                count += objects[i].Count;

            return count;
        }
        public int GetTotalStaticObjectCount()
        {
            return objects[0].Count;
        }

        public List<GameObject> GetAllObjects()
        {
            List<GameObject> tmpList = new List<GameObject>();

            for (int i = 1; i < objects.Length; i++)
                tmpList.AddRange(objects[i]);

            return tmpList;
        }
        public List<GameObject> GetAllObjectsInView()
        {
            List<GameObject> tmpList = new List<GameObject>();

            for (int i = 1; i < objects.Length; i++)
                tmpList.AddRange(objects[i].Where(item => game.camera.IsInCameraView(item)));

            return tmpList;
        }
        public List<GameObject> GetAllObjectsIncLayerZero()
        {
            List<GameObject> tmpList = new List<GameObject>();

            for (int i = 0; i < objects.Length; i++)
                tmpList.AddRange(objects[i]);

            return tmpList;
        }

        internal GameObject isBlocked(GameObject g, Rectangle r, Vector2 velocity)
        {
            foreach (GameObject go in (UseBackgroundLayerCollision ? GetAllObjectsIncLayerZero() : GetAllObjects()))
            {
                if (!go.EnableCollisionDetection || !go.Enabled) continue;
                if (g != null && go == g) continue;

                // Colision per layer check
                if (g != null)
                    if (LayerSetting(g.Layer).IgnoreCollisionLayer[go.Layer])
                        continue;

                Rectangle checkBounds = go.Bounds;

                if (r.IntersectsWith(checkBounds))
                {
                    // Colision Here
                    if (g != null)
                    {
                        g.OnCollision(go, velocity);
                        go.OnCollision(g, velocity);
                    }

                    if(!go.IsTrigger && !g.IsTrigger)
                        return go;
                }
            }

            return null;
        }

        internal void ChangeObjectChunk(GameObject gameObject, Vector2 _chunkLoc)
        {
            // TODO: GameObject => ObjectChunks
        }
    }

    internal class TempObject
    {
        public GameObject gameObject;
        public int layerID;

        public TempObject(int layerID, GameObject gameObject)
        {
            this.gameObject = gameObject;
            this.layerID = layerID;
        }
    }
    internal class ChunkGraphic
    {
        [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
        public static extern System.IntPtr SelectObject(
            [In()] System.IntPtr hdc,
            [In()] System.IntPtr h);

        public Bitmap b;
        public Graphics g;

        internal IntPtr hdcSrc = IntPtr.Zero;
        internal IntPtr hBitmap = IntPtr.Zero;
        internal IntPtr hOldObject = IntPtr.Zero;

        public ChunkGraphic(Bitmap b, Graphics g)
        {
            this.b = b;
            this.g = g;
        }

        public void Recalc()
        {
            hBitmap = b.GetHbitmap();
            hdcSrc = g.GetHdc();
            hOldObject = SelectObject(hdcSrc, hBitmap);
        }
    }
}
