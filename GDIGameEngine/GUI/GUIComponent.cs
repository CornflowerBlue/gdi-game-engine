﻿using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GDIGameEngine.GUI
{
    public class GUIComponent
    {
        public Sprite sprite;
        public Vector2 location;
        public Rectangle Bounds = Rectangle.Empty;
        public bool IsClickable = false;

        public bool IsEnabled = true;

        public Game game { get; internal set; }


        public GUIComponent(Sprite sprite, Vector2 location)
        {
            this.sprite = sprite;
            this.location = location;

            if (sprite != null)
                Bounds = new Rectangle(0, 0, sprite.Image.Width, sprite.Image.Height);
        }

        public virtual void OnLeftMouseClick()
        {

        }

        public virtual void OnUpdate(float DeltaTime)
        {

        }
        public virtual void OnRender(Graphics g)
        {
            if (sprite != null || !IsEnabled)
               g.DrawImageUnscaled(sprite, location.GetPoint());
        }
    }
}
