﻿using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GDIGameEngine.GUI.Components
{
    public sealed class TextComponent : GUIComponent
    {
        public Font font;
        public string text;
        public Brush brush;

        /// <summary>
        /// Does not automaticly create a hitbox
        /// </summary>
        public TextComponent(string Text, Vector2 location, Brush brush, Font font = null)
            : base(null, location)
        {
            this.text = Text;
            this.brush = brush;
            this.font = (font == null ? new Font("Arial", 14f) : font);

            Bitmap b = new Bitmap(256, 256);
            Graphics g = Graphics.FromImage(b);

            Bounds = new Rectangle(new Point(0,0), g.MeasureString(Text, this.font).ToSize());
        }

        public override void OnRender(Graphics g)
        {
            g.DrawString(text, font, brush, location);
        }
    }
}
