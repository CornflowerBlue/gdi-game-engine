﻿using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDIGameEngine.GUI.Components
{
    public delegate void OnClick();

    public class ButtonComponent : GUIComponent
    {
        public event OnClick OnLeftClick;

        public ButtonComponent(Sprite sprite, Vector2 location)
            : base(sprite, location)
        {
            IsClickable = true;
        }

        public override void OnLeftMouseClick()
        {
            if (OnLeftClick != null) OnLeftClick.Invoke();

            base.OnLeftMouseClick();
        }
    }
}
