﻿using GDIGameEngine.Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GDIGameEngine.GUI
{
    public class GUI
    {
        private Scene scene;

        internal List<GUIComponent> GUIComponents = new List<GUIComponent>();

        public GUI(Scene scene)
        {
            this.scene = scene;
        }

        public GUIComponent AddComponent(GUIComponent c)
        {
            c.game = scene.game;
            GUIComponents.Add(c);

            return c;
        }
        public void RemoveComponent(GUIComponent c)
        {
            GUIComponents.Remove(c);
        }

        public void Update(float DeltaTime)
        {
            List<GUIComponent> _GUIComponents = new List<GUIComponent>();
            _GUIComponents.AddRange(GUIComponents);

            foreach (GUIComponent g in _GUIComponents)
                g.OnUpdate(DeltaTime);
        }
        public void Render(Graphics graphic)
        {
            List<GUIComponent> _GUIComponents = new List<GUIComponent>();
            _GUIComponents.AddRange(GUIComponents);

            foreach (GUIComponent g in _GUIComponents.Where(c => c.IsEnabled))
                g.OnRender(graphic);
        }

        internal GUIComponent InterceptsWithAnyComponent(Rectangle Rect)
        {
            List<GUIComponent> _GUIComponents = new List<GUIComponent>();
            _GUIComponents.AddRange(GUIComponents);

            foreach (GUIComponent g in _GUIComponents)
            {
                if (!g.IsClickable) continue;
                if (!g.IsEnabled) continue;

                if((g.Bounds + g.location).IntersectsWith(Rect))
                    return g;
            }

            return null;
        }
    }
}
