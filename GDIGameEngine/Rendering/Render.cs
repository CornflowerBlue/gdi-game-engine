﻿using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace GDIGameEngine.Rendering
{
    public static class Render
    {
        [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
        private static extern System.IntPtr SelectObject(
            [In()] System.IntPtr hdc,
            [In()] System.IntPtr h);

        [DllImport("gdi32.dll", EntryPoint = "BitBlt")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool BitBlt(
            [In()] System.IntPtr hdc, int x, int y, int cx, int cy,
            [In()] System.IntPtr hdcSrc, int x1, int y1, uint rop);

        [DllImport("gdi32.dll")]
        static extern bool StretchBlt(IntPtr hdcDest, int nXOriginDest, int nYOriginDest,
            int nWidthDest, int nHeightDest,
            IntPtr hdcSrc, int nXOriginSrc, int nYOriginSrc, int nWidthSrc, int nHeightSrc,
            uint dwRop);
        [DllImport("gdi32.dll")]
        static extern bool PlgBlt(IntPtr hdcDest, Point[] lpPoint, IntPtr hdcSrc,
           int nXSrc, int nYSrc, int nWidth, int nHeight, IntPtr hbmMask, int xMask,
           int yMask);

        const float HalfPi = (float)(Math.PI * 0.5);

        public static void RenderGameObject(Game game, Graphics g, GameObject gameObject)
        {
            if (gameObject.Sprite != null)
            {
                if (gameObject.Sprite.ContainsAlpha || gameObject.Rotation != 0)
                {
                    g.DrawImage(gameObject.Sprite,
                        (int)(gameObject.DrawPosition.X * game.camera.Scale), 
                        (int)(gameObject.DrawPosition.Y * game.camera.Scale), 
                        (int)(gameObject.SpriteSize.Width * game.camera.Scale), 
                        (int)(gameObject.SpriteSize.Height * game.camera.Scale));
                }
                else
                {
                    IntPtr hdcDest = g.GetHdc();
                    try
                    {
                        if (gameObject.Rotation == 0 || true)
                        {
                            StretchBlt(hdcDest,
                                (int)(gameObject.DrawPosition.X * game.camera.Scale),
                                (int)(gameObject.DrawPosition.Y * game.camera.Scale),
                                (int)(gameObject.SpriteSize.Width * game.camera.Scale),
                                (int)(gameObject.SpriteSize.Height * game.camera.Scale),
                                gameObject.Sprite.pSource, 0, 0,
                                (int)(gameObject.SpriteSize.Width),
                                (int)(gameObject.SpriteSize.Height),
                                0x00CC0020);
                        }
                        //else // PlgBlt Code
                        //{
                        //    float HalfWidth = gameObject.Sprite.Image.Size.Width / 2;
                        //    float HalfHeight = gameObject.Sprite.Image.Size.Height / 2;

                        //    float AngleRad = (float)(Math.PI * gameObject.Rotation / 180.0f);
                        //    int inX = (int)gameObject.DrawPosition.X;
                        //    int inY = (int)gameObject.DrawPosition.Y;

                        //    Point origin = (gameObject.DrawPosition + gameObject.Origin).GetPoint();

                        //    Point[] pts = new Point[4];
                        //    pts[0] = Helper.Helper.RotatePoint(new Point(inX, inY), origin, AngleRad);
                        //    pts[1] = Helper.Helper.RotatePoint(new Point(inX + gameObject.Sprite.Image.Width, inY), origin, AngleRad);
                        //    pts[2] = Helper.Helper.RotatePoint(new Point(inX, inY + gameObject.Sprite.Image.Height), origin, AngleRad);
                        //    pts[3] = Helper.Helper.RotatePoint(new Point(inX + gameObject.Sprite.Image.Width, inY + gameObject.Sprite.Image.Height), origin, AngleRad);

                        //    PlgBlt(hdcDest, pts, gameObject.Sprite.pSource,
                        //        0,
                        //        0,
                        //        (int)(gameObject.SpriteSize.Width * game.camera.Scale),
                        //        (int)(gameObject.SpriteSize.Height * game.camera.Scale),
                        //        gameObject.Sprite.pSourceInv,
                        //        0,
                        //        0);
                        //}

                    }
                    finally
                    {
                        if (hdcDest != IntPtr.Zero) g.ReleaseHdc(hdcDest);
                    }
                }
            }
        }

        internal static void RenderBackgroundChunk(Game game, Graphics g, ChunkGraphic chunk, Vector2 chunkPos)
        {
            IntPtr hdcDest = g.GetHdc();
            try
            {
                StretchBlt(hdcDest,
                    (int)(chunkPos.GetPoint().X * game.camera.Scale),
                    (int)(chunkPos.GetPoint().Y * game.camera.Scale),
                    (int)(Scene.ChunkSize.Width * game.camera.Scale),
                    (int)(Scene.ChunkSize.Height * game.camera.Scale),
                    chunk.hdcSrc, 0, 0,
                    (int)(Scene.ChunkSize.Width),
                    (int)(Scene.ChunkSize.Height),
                    0x00CC0020);
            }
            finally
            {
                if (hdcDest != IntPtr.Zero) g.ReleaseHdc(hdcDest);
            }
        }
    }
}
