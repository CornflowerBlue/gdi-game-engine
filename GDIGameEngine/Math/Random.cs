﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDIGameEngine.GDIMath
{
    public static class Random
    {
        public static int seed = -1;

        public static int Range(int min, int max)
        {
            if (max != int.MaxValue) max++;
            if (seed == -1) seed = new System.Random().Next(int.MinValue, int.MaxValue);

            int newNumber = new System.Random(seed).Next(min, max);
            seed = -1;

            return newNumber;
        }

        /// <summary>
        /// This will create a random seed and return a random number.
        /// It is recomended to use Random.Range
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns>A Random number</returns>
        public static int Range2(int min, int max)
        {
            if (max != int.MaxValue) max++;

            byte[] seed = Guid.NewGuid().ToByteArray();
            for (int i = 0; i < 3; i++)
            {
                seed[i] ^= seed[i + 4];
                seed[i] ^= seed[i + 8];
                seed[i] ^= seed[i + 12];
            }

            int seedInt = BitConverter.ToInt32(seed, 12);

            System.Random random = new System.Random(seedInt);
            return random.Next(min, max);
        }

        public static float Range(float min, float max)
        {
            if (seed == -1) seed = new System.Random().Next(int.MinValue, int.MaxValue);

            float newNumber = (max - min) * (float)new System.Random(seed).NextDouble() + min;
            seed = -1;

            return newNumber;
        }
    }
}
