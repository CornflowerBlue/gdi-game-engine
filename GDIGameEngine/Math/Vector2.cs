﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDIGameEngine.GDIMath
{
    public class Vector2 : IDisposable
    {
        // Static
        public static Vector2 Zero
        {
            get { return Empty; }
        }
        public static Vector2 Empty
        {
            get { return new Vector2(0, 0); }
        }
        public static Vector2 One
        {
            get { return new Vector2(1, 1); }
        }
        public static Vector2 Up
        {
            get { return new Vector2(0, -1); }
        }
        public static Vector2 Down
        {
            get { return new Vector2(0, 1); }
        }
        public static Vector2 Left
        {
            get { return new Vector2(-1, 0); }
        }
        public static Vector2 Right
        {
            get { return new Vector2(1, 0); }
        }

        public float X;
        public float Y;

        public Vector2(float X, float Y)
        {
            this.X = X;
            this.Y = Y;
        }
        public Vector2(float XY)
        {
            this.X = XY;
            this.Y = XY;
        }

        public override string ToString()
        {
            return "{" + string.Format("{0}, {1}", X, Y) + "}";
        }
        public Point GetPoint()
        {
            return new Point((int)X, (int)Y);
        }

        public float Distance(Vector2 v2)
        {
            return (float)Math.Sqrt(Math.Pow((double)v2.X - (double)X, 2) + Math.Pow((double)v2.Y - (double)Y, 2));
        }
        public static float Distance(Vector2 v1, Vector2 v2)
        {
            return (float)Math.Sqrt(Math.Pow((double)v2.X - (double)v1.X, 2) + Math.Pow((double)v2.Y - (double)v1.Y, 2));
        }

        public void Normalize()
        {
            float distance = (float)Math.Sqrt(X * X + Y * Y);
            X = X / distance;
            Y /= distance;
        }

        public static Vector2 Lerp(Vector2 value1, Vector2 value2, float amount)
        {
            amount = Helper.Helper.Clamp<float>(amount, 0f, 1f);
            return value1 + (value2 - value1) * amount;
        }

        // Operators
        public static Vector2 operator +(Vector2 c1, Vector2 c2)
        {
            return new Vector2(c1.X + c2.X, c1.Y + c2.Y);
        }
        public static Vector2 operator +(Vector2 c1, int c2)
        {
            return new Vector2(c1.X + c2, c1.Y + c2);
        }
        public static Vector2 operator +(Vector2 c1, float c2)
        {
            return new Vector2(c1.X + c2, c1.Y + c2);
        }
        public static Vector2 operator +(Vector2 c1, Point c2)
        {
            return new Vector2(c1.X + c2.X, c1.Y + c2.Y);
        }
        public static Vector2 operator +(Vector2 c1, PointF c2)
        {
            return new Vector2(c1.X + c2.X, c1.Y + c2.Y);
        }
        public static Rectangle operator +(Rectangle c1, Vector2 c2)
        {
            return new Rectangle((int)(c1.X + c2.X), (int)(c1.Y + c2.Y), c1.Width, c1.Height);
        }
        public static Rectangle operator +(Vector2 c1, Rectangle c2)
        {
            return new Rectangle((int)(c1.X + c2.X), (int)(c1.Y + c2.Y), c2.Width, c2.Height);
        }
        public static Vector2 operator +(Vector2 c1, Size c2)
        {
            return new Vector2(c1.X + c2.Width, c1.Y + c2.Height);
        }

        public static Vector2 operator -(Vector2 c1)
        {
            return new Vector2(-c1.X, -c1.Y);
        }
        public static Vector2 operator -(Vector2 c1, Vector2 c2)
        {
            return new Vector2(c1.X - c2.X, c1.Y - c2.Y);
        }
        public static Vector2 operator -(Vector2 c1, int c2)
        {
            return new Vector2(c1.X - c2, c1.Y - c2);
        }
        public static Vector2 operator -(Vector2 c1, float c2)
        {
            return new Vector2(c1.X - c2, c1.Y - c2);
        }
        public static Vector2 operator -(Vector2 c1, Point c2)
        {
            return new Vector2(c1.X - c2.X, c1.Y - c2.Y);
        }
        public static Vector2 operator -(Vector2 c1, PointF c2)
        {
            return new Vector2(c1.X - c2.X, c1.Y - c2.Y);
        }
        public static Rectangle operator -(Rectangle c1, Vector2 c2)
        {
            return new Rectangle((int)(c1.X - c2.X), (int)(c1.Y - c2.Y), c1.Width, c1.Height);
        }
        public static Rectangle operator -(Vector2 c1, Rectangle c2)
        {
            return new Rectangle((int)(c1.X - c2.X), (int)(c1.Y - c2.Y), c2.Width, c2.Height);
        }
        public static Vector2 operator -(Vector2 c1, Size c2)
        {
            return new Vector2(c1.X - c2.Width, c1.Y - c2.Height);
        }

        public static Vector2 operator *(Vector2 c1, Vector2 c2)
        {
            return new Vector2(c1.X * c2.X, c1.Y * c2.Y);
        }
        public static Vector2 operator *(Vector2 c1, int c2)
        {
            return new Vector2(c1.X * c2, c1.Y * c2);
        }
        public static Vector2 operator *(Vector2 c1, float c2)
        {
            return new Vector2(c1.X * c2, c1.Y * c2);
        }
        public static Vector2 operator *(Vector2 c1, Point c2)
        {
            return new Vector2(c1.X * c2.X, c1.Y * c2.Y);
        }
        public static Vector2 operator *(Vector2 c1, PointF c2)
        {
            return new Vector2(c1.X * c2.X, c1.Y * c2.Y);
        }
        public static Rectangle operator *(Rectangle c1, Vector2 c2)
        {
            return new Rectangle((int)(c1.X * c2.X), (int)(c1.Y * c2.Y), c1.Width, c1.Height);
        }
        public static Rectangle operator *(Vector2 c1, Rectangle c2)
        {
            return new Rectangle((int)(c1.X * c2.X), (int)(c1.Y * c2.Y), c2.Width, c2.Height);
        }
        public static Vector2 operator *(Vector2 c1, Size c2)
        {
            return new Vector2(c1.X * c2.Width, c1.Y * c2.Height);
        }

        public static Vector2 operator /(Vector2 c1, Vector2 c2)
        {
            return new Vector2(c1.X / c2.X, c1.Y / c2.Y);
        }
        public static Vector2 operator /(Vector2 c1, int c2)
        {
            return new Vector2(c1.X / c2, c1.Y / c2);
        }
        public static Vector2 operator /(Vector2 c1, float c2)
        {
            return new Vector2(c1.X / c2, c1.Y / c2);
        }
        public static Vector2 operator /(Vector2 c1, Point c2)
        {
            return new Vector2(c1.X / c2.X, c1.Y / c2.Y);
        }
        public static Vector2 operator /(Vector2 c1, PointF c2)
        {
            return new Vector2(c1.X / c2.X, c1.Y / c2.Y);
        }
        public static Rectangle operator /(Rectangle c1, Vector2 c2)
        {
            return new Rectangle((int)(c1.X / c2.X), (int)(c1.Y / c2.Y), c1.Width, c1.Height);
        }
        public static Rectangle operator /(Vector2 c1, Rectangle c2)
        {
            return new Rectangle((int)(c1.X / c2.X), (int)(c1.Y / c2.Y), c2.Width, c2.Height);
        }
        public static Vector2 operator /(Vector2 c1, Size c2)
        {
            return new Vector2(c1.X / c2.Width, c1.Y / c2.Height);
        }

        public static bool operator ==(Vector2 rational1, Vector2 rational2)
        {
            return rational1.X == rational2.X && rational1.Y == rational2.Y;
        }
        public static bool operator !=(Vector2 rational1, Vector2 rational2)
        {
            if (rational1.GetType() == typeof(Nullable)) return false;
            return rational1.X != rational2.X || rational1.Y != rational2.Y;
        }
        
        public override bool Equals(object _obj)
        {
            if (_obj == null) return false;

            Vector2 obj = (Vector2)_obj;

            return this.X == obj.X && this.Y == obj.Y;
        }
        public override int GetHashCode()
        {
            return X.GetHashCode() + Y.GetHashCode();
        }
        public static implicit operator PointF(Vector2 c1)
        {
            return new PointF(c1.X, c1.Y);
        }

        public void Dispose()
        {
            
        }
    }
}
