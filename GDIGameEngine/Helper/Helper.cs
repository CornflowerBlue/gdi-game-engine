﻿using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace GDIGameEngine.Helper
{
    public class Helper
    {
        [DllImport("gdi32.dll")]
        static extern bool PlgBlt(IntPtr hdcDest, Point[] lpPoint, IntPtr hdcSrc, 
            int nXSrc, int nYSrc, int nWidth, int nHeight, IntPtr hbmMask, int xMask, int yMask);
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern IntPtr CreateCompatibleDC(IntPtr hdc);
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern bool DeleteDC(IntPtr hdc);
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        static extern bool DeleteObject(IntPtr hObject);
        [DllImport("gdi32.dll")]
        static extern int GetObject(IntPtr hgdiobj, int cbBuffer, IntPtr lpvObject);


        public static Vector2 GetAngleVBetweenObjects(GameObject go1, GameObject go2)
        {
            return GetAngleVBetweenObjects(go1.Position, go2.Position);
        }
        public static float GetAngleFBetweenObjects(GameObject go1, GameObject go2)
        {
            return GetAngleFBetweenObjects(go1.Position, go2.Position);
        }
        public static Vector2 GetAngleVBetweenObjects(Vector2 go1, Vector2 go2)
        {
            float angle = (float)Math.Atan2((double)(go1.Y - go2.Y), (double)(go1.X - go2.X));

            return new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
        }
        public static float GetAngleFBetweenObjects(Vector2 go1, Vector2 go2)
        {
            float angle = (float)Math.Atan2((double)(go1.Y - go2.Y), (double)(go1.X - go2.X));

            return angle;
        }

        public static Vector2 AngleToVector(float angle)
        {
            return new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
        }

        internal static Bitmap ResizeBitmap(Bitmap sourceBMP, int width, int height)
        {
            Bitmap result = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(result))
            {
                g.ToHighQuality();
                g.DrawImage(sourceBMP, 0, 0, width, height);
            }
            return result;
        }

        internal static Bitmap Crop(Bitmap image, Rectangle r)
        {
            Bitmap bmpCrop;
            using (Bitmap bmpImage = new Bitmap(image))
            {
                bmpCrop = bmpImage.Clone(r, bmpImage.PixelFormat);
                bmpImage.Dispose();
            }

            return (bmpCrop);
        }

        internal static void _RotateImage(ref Bitmap img, Bitmap baseImg, float rotationAngle)
        {
            int minx = int.MaxValue, maxx = int.MinValue, miny = int.MaxValue, maxy = int.MinValue;
            float AngleRad = (float)(Math.PI * rotationAngle / 180.0f);
            Point origin = new Point(img.Width / 2, img.Height / 2);

            Point[] pts = new Point[4];
            pts[0] = RotatePoint(new Point(0, 0), origin, AngleRad);
            pts[1] = RotatePoint(new Point(0 + img.Width, 0), origin, AngleRad);
            pts[2] = RotatePoint(new Point(0, 0 + img.Height), origin, AngleRad);
            pts[3] = RotatePoint(new Point(0 + img.Width, 0 + img.Height), origin, AngleRad);

            foreach (Point pt in pts)
            {
                minx = Math.Min(minx, pt.X);
                maxx = Math.Max(maxx, pt.X);
                miny = Math.Min(miny, pt.Y);
                maxy = Math.Max(maxy, pt.Y);
            }

            img = new Bitmap(img.Width, img.Height);
            using (Graphics g = Graphics.FromImage(img))
            {
                g.Clear(Color.Transparent);

                IntPtr pTarget = g.GetHdc();
                IntPtr pSource = CreateCompatibleDC(pTarget);
                SelectObject(pSource, baseImg.GetHbitmap());

                PlgBlt(pTarget, pts, pSource,
                    0, 0,
                    img.Width, img.Height,
                    IntPtr.Zero, 0, 0);

                g.ReleaseHdc(pTarget);
                DeleteDC(pSource);
            }
        }

        internal static Point RotatePoint(Point pointToRotate, Point centerPoint, double angleInRadians)
        {
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);
            return new Point
            {
                X =
                    (int)
                    (cosTheta * (pointToRotate.X - centerPoint.X) -
                    sinTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.X),
                Y =
                    (int)
                    (sinTheta * (pointToRotate.X - centerPoint.X) +
                    cosTheta * (pointToRotate.Y - centerPoint.Y) + centerPoint.Y)
            };
        }

        internal static void RotateImage(ref Bitmap img, Bitmap baseImg, float rotationAngle)
        {
            int minx = int.MaxValue, maxx = int.MinValue, miny = int.MaxValue, maxy = int.MinValue;
            float AngleRad = (float)(Math.PI * rotationAngle / 180.0f);
            Point origin = new Point(baseImg.Width / 2, baseImg.Height / 2);

            Point[] pts = new Point[4];
            pts[0] = RotatePoint(new Point(0, 0), origin, AngleRad);
            pts[1] = RotatePoint(new Point( baseImg.Width, 0), origin, AngleRad);
            pts[2] = RotatePoint(new Point(0, baseImg.Height), origin, AngleRad);
            pts[3] = RotatePoint(new Point(baseImg.Width, baseImg.Height), origin, AngleRad);

            foreach (Point pt in pts)
            {
                minx = Math.Min(minx, pt.X);
                maxx = Math.Max(maxx, pt.X);
                miny = Math.Min(miny, pt.Y);
                maxy = Math.Max(maxy, pt.Y);
            }

            
            img = new Bitmap(maxx - minx, maxy - miny);
            using (Graphics g = Graphics.FromImage(img))
            {
                g.TranslateTransform((float)img.Width / 2, (float)img.Height / 2);
                g.RotateTransform(rotationAngle);
                g.TranslateTransform(-(float)img.Width / 2, -(float)img.Height / 2);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(baseImg, img.Width / 2 - baseImg.Width / 2, img.Height / 2 - baseImg.Height / 2);
            }
        }
        
        public static T Clamp<T>(T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0) return min;
            else if (val.CompareTo(max) > 0) return max;
            else return val;
        }

        internal static Bitmap TintBitmap(Bitmap b, Color tintColor)
        {
            return b;
            ImageAttributes ia = new ImageAttributes();
            float tR = (float)tintColor.R / 255;
            float tG = (float)tintColor.G / 255;
            float tB = (float)tintColor.B / 255;
            float tA = (float)tintColor.A / 255;

            ColorMatrix cm = new ColorMatrix(new float[][]
            {
                new float[] {1, 0, 0, 0, 0},
                new float[] {0, 1, 0, 0, 0},
                new float[] {0, 0, 1, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {tR, tG, tB, tA, 1}
            });

            ia.SetColorMatrix(cm, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            Bitmap result = new Bitmap(b.Width, b.Height);
            Point[] pts = new Point[] { new Point(0, 0), new Point(b.Width, 0), new Point(0, b.Height) };
            Rectangle rect = new Rectangle(0, 0, b.Width, b.Height);

            using (Graphics g = Graphics.FromImage((Image)result))
            {
                g.DrawImage(b, pts, rect, GraphicsUnit.Pixel, ia);
            }

            return result;
        }

        internal static float Lerp(float value1, float value2, float amount)
        {
            return value1 + (value2 - value1) * amount;
        }

        internal static Image Base64ToImage(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        internal static string CalculateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        internal static bool IsAlphaBitmap(Bitmap bmp)
        {
            for (int y = 0; y < bmp.Height; ++y)
            {
                for (int x = 0; x < bmp.Width; ++x)
                {
                    if (bmp.GetPixel(x, y).A != 255)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
