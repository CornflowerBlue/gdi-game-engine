﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GDIGameEngine.Particles
{
    public sealed class ParticleSystem
    {
        private Game game { get; set; }

        public List<Particle> particles = new List<Particle>();

        public ParticleSystem(Game game)
        {
            this.game = game;
        }

        public void Update(float DeltaTime)
        {
            List<Particle> removeParticles = new List<Particle>();
            List<Particle> _particles = new List<Particle>();
            _particles.AddRange(particles);

            foreach (Particle p in _particles)
            {
                if (p.dead)
                {
                    removeParticles.Add(p);
                    continue;
                }
                p.OnTick(DeltaTime);
            }

            foreach (Particle p in removeParticles) particles.Remove(p);
            removeParticles.Clear();
        }

        public int Draw(Graphics g)
        {
            int RenderCount = 0;

            foreach (Particle p in particles)
            {
                if (p.dead) continue;
                if (game.UseOcclusionCulling && !game.camera.IsInCameraView(p.DrawPosition + game.camera.position, (int)p.Size, (int)p.Size))
                    continue;
                RenderCount++;

                p.OnDraw(g);
            }

            return RenderCount;
        }
    }
}
