﻿using GDIGameEngine.GDIMath;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GDIGameEngine.Particles
{
    public delegate void OnDead(Particle p);
    public class Particle
    {
        // Effects
        public static event OnDead On_Dead;

        public Game game { get; private set; }
        private List<IParticleEffect> Effects = new List<IParticleEffect>();

        private Vector2 _Position = Vector2.Empty;
        public Vector2 Position { get { return _Position; } set { _Position = value; } }

        public Vector2 DrawPosition { get { return Position + (Parent == null ? Vector2.Empty : Parent.Position) - game.camera.position; } }

        private Color _Color = Color.Red;
        public Color Color { get { return _Color; } set { _Color = value; } }

        private float _Speed = 300;
        public float Speed { get { return _Speed; } set { _Speed = value; } }

        private float _Size = 1f;
        public float Size { get { return _Size; } set { _Size = value; } }

        private GameObject _Parent;
        public GameObject Parent { get { return _Parent; } set { _Parent = value; } }

        private Vector2 _Velocity = Vector2.Empty;
        public Vector2 Velocity { get { return _Velocity; } set { _Velocity = value; } }

        internal bool dead = false;

        public float lifeTime = 0.1f;
        private float currentTime;

        public Particle(Game game, Vector2 pos, Color color)
        {
            this.game = game;
            this.Position = pos;
            this.Color = color;

            OnCreate();
        }
        ~Particle()
        {
            OnDead();
        }

        // OnCreate
        public virtual void OnCreate() { }

        // OnDead
        public virtual void OnDead() 
        {
            if (On_Dead != null) On_Dead.Invoke(this);
        }

        // OnMove
        public virtual void OnMove() { }

        // OnTick
        public virtual void OnTick(float DeltaTime) 
        {
            currentTime += DeltaTime;

            if (currentTime > lifeTime)
            {
                OnDead();
                dead = true;
                return;
            }
            else
            {
                Position += Velocity * Speed * DeltaTime;
                if (Velocity != Vector2.Zero) OnMove();
            }

            foreach (IParticleEffect p in Effects)
                p.ApplyEffect(DeltaTime, this);
        }

        // OnDraw
        public virtual void OnDraw(Graphics g) 
        {
            Vector2 drawPosition = this.DrawPosition - (Size / 2);
            g.FillEllipse(new SolidBrush(Color), drawPosition.X, drawPosition.Y, Size, Size);
        }

        // Effects
        public void AddEffect(IParticleEffect effect)
        {
            Effects.Add(effect);
            effect.StartEffect(this);
        }
    }
}
