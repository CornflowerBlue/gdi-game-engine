﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Random = GDIGameEngine.GDIMath.Random;

namespace GDIGameEngine.Particles
{
    class RandomColorEffect : IParticleEffect
    {
        public void StartEffect(Particle p)
        {
            
        }

        public void ApplyEffect(float DeltaTime, Particle p)
        {
            int R, G, B;

            Random.seed = Environment.TickCount * 234 + GetHashCode();
            R = Random.Range(0, 255);
            Random.seed = Environment.TickCount * 24 + GetHashCode();
            G = Random.Range(0, 255);
            Random.seed = Environment.TickCount * 14 + GetHashCode();
            B = Random.Range(0, 255);

            p.Color = Color.FromArgb(p.Color.A, R, G, B);
        }
    }
}
