﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GDIGameEngine.Particles
{
    class SizeOverLifeEffect : IParticleEffect
    {
        float size, _oldSize;
        float startTime;

        float curTime;

        public SizeOverLifeEffect(float size, float startTime)
        {
            this.startTime = startTime;
            this.size = size;
        }

        public void StartEffect(Particle p)
        {
            _oldSize = p.Size;
        }

        public void ApplyEffect(float DeltaTime, Particle p)
        {
            curTime += DeltaTime;

            if(curTime > startTime)
                p.Size = Helper.Helper.Lerp(_oldSize, size, (curTime - startTime) / (p.lifeTime - startTime));
        }
    }
}
