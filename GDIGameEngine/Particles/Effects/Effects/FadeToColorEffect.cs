﻿using GDIGameEngine.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GDIGameEngine.Particles
{
    class FadeToColorEffect : IParticleEffect
    {
        Color _startColor;
        Color endColor;

        float curTime = 0f;

        public FadeToColorEffect(Color endColor)
        {
            this.endColor = endColor;
        }

        public void StartEffect(Particle p)
        {
            _startColor = p.Color;
        }

        public void ApplyEffect(float DeltaTime, Particle p)
        {
            curTime += DeltaTime;

            float a = curTime / p.lifeTime;
            float oldA = p.Color.A;
            p.Color = ColorExtension.Lerp(_startColor, endColor, a);
            p.Color = Color.FromArgb((int)oldA, p.Color.R, p.Color.G, p.Color.B);
        }
    }
}
