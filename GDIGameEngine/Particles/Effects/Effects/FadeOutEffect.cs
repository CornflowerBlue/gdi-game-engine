﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GDIGameEngine.Particles
{
    class FadeOutEffect : IParticleEffect
    {
        float startFade = 1f;
        float endFade = 2f;

        float curTime = 0f;

        public FadeOutEffect(float startFade, float endFade)
        {
            this.startFade = startFade;
            this.endFade = endFade;
        }

        public void StartEffect(Particle p)
        {
            p.lifeTime = endFade;
        }

        public void ApplyEffect(float DeltaTime, Particle p)
        {
            curTime += DeltaTime;

            if (curTime >= endFade)
            {
                p.dead = true;
                return;
            }

            if (curTime >= startFade)
            {
                int newAlpha = (int)Helper.Helper.Lerp(255, 0, (curTime - startFade) / (endFade - startFade));
                p.Color = Color.FromArgb(newAlpha, p.Color.R, p.Color.G, p.Color.B);
            }
        }
    }
}
