﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GDIGameEngine.Particles
{
    public static class Effects
    {
        public static IParticleEffect FadeOutEffect(float startFade, float endFade)
        {
            return new FadeOutEffect(startFade, endFade);
        }

        public static IParticleEffect SizeOverLifeEffect(float size, float startTime)
        {
            return new SizeOverLifeEffect(size, startTime);
        }

        public static IParticleEffect FadeToColorEffect(Color color)
        {
            return new FadeToColorEffect(color);
        }

        public static IParticleEffect RandomColorEffect()
        {
            return new RandomColorEffect();
        }
    }
}
