﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDIGameEngine.Particles
{
    public interface IParticleEffect
    {
        void StartEffect(Particle p);
        void ApplyEffect(float DeltaTime, Particle p);
    }
}
