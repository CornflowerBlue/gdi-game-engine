﻿using GDIGameEngine.GDIMath;
using GDIGameEngine.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GDIGameEngine.Base
{
    public class Camera
    {
        public Vector2 position = Vector2.Empty;

        private float _Scale = 1f;
        public float Scale { get { return _Scale; } set { _Scale = value; } }

        private Game game;
        private float speed = 300;

        public Camera(Game game)
        {
            this.game = game;
        }

        public void LookAt(GameObject go)
        {
            position = go.Position - new Vector2(game.RenderWidth / 2, game.RenderHeight / 2) + new Vector2(go.Sprite.Base_image.Width / 2, go.Sprite.Base_image.Height / 2);
        }
        public void LookAt(GameObject go, Vector2 offset)
        {
            position = go.Position - new Vector2(game.RenderWidth / 2, game.RenderHeight / 2) + new Vector2(go.Sprite.Base_image.Width / 2, go.Sprite.Base_image.Height / 2) + offset;
        }
        public void LookAt(Vector2 v)
        {
            position = v - new Vector2(game.RenderWidth / 2, game.RenderHeight / 2);
        }

        internal void Update(float deltaTime)
        {
            if (Keyboard.IsKeyDown(Keys.Down))
            {
                position += Vector2.Down * deltaTime * speed;
            }
            if (Keyboard.IsKeyDown(Keys.Up))
            {
                position += Vector2.Up * deltaTime * speed;
            }
            if (Keyboard.IsKeyDown(Keys.Left))
            {
                position += Vector2.Left * deltaTime * speed;
            }
            if (Keyboard.IsKeyDown(Keys.Right))
            {
                position += Vector2.Right * deltaTime * speed;
            }
        }

        public Vector2 ScreenToWorld(Vector2 screenLocation)
        {
            return ((screenLocation / Scale) + new Vector2((int)position.X, (int)position.Y));
        }

        public Rectangle CameraRectangle()
        {
            Rectangle cR = new Rectangle((int)position.X, (int)position.Y, (int)(game.RenderWidth / Scale), (int)(game.RenderHeight / Scale));
            return cR;
        }

        internal bool IsInCameraView(Vector2 pos, int width, int height)
        {
            Rectangle r = new Rectangle((int)pos.X, (int)pos.Y, width, height);
            Rectangle cR = CameraRectangle();

            return r.IntersectsWith(cR);
        }
        public bool IsInCameraView(GameObject gameObject)
        {
            return IsInCameraView(gameObject.Position - gameObject.Origin, gameObject.SpriteBounds.Width, gameObject.SpriteBounds.Height);
        }
    }
}
