﻿using GDIGameEngine.Base;
using GDIGameEngine.GDIMath;
using GDIGameEngine.Input;
using GDIGameEngine.Particles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GDIGameEngine
{
    public delegate void MoveEvent(GameObject o, Vector2 Old);
    public delegate void RotateEvent(GameObject o, float Old);

    public class GameObject : IDisposable
    {
        // Events
        public event MoveEvent OnMoveEvent;
        public event RotateEvent OnRotateEvent;

        public Game game 
        { get; internal set; }

        private Sprite _Sprite;
        public Sprite Sprite 
        { get { return _Sprite; } set { _Sprite = value; if (value != null) if (Hitbox == new Rectangle(0, 0, 0, 0)) Hitbox = new Rectangle(0, 0, value.Image.Width, value.Image.Height); } }

        public int Layer 
        { get; internal set; }
        public uint ObjectID 
        { get; internal set; }

        internal Vector2 ChunkLoc = Vector2.Empty;

        private Vector2 Old_Position;
        private Vector2 _Position = Vector2.Empty;
        public Vector2 Position
        {
            get
            {
                return (_Position) + (Parent == null ? Vector2.Empty : Parent.Position);
            }
            private set
            {
                _Position = value;
                Vector2 _chunkLoc = new Vector2((int)(Position.X / 320), (int)(Position.Y / 320));
                if (_chunkLoc != ChunkLoc)
                {
                    if(game != null)
                        game.scene.ChangeObjectChunk(this, _chunkLoc);
                    ChunkLoc = _chunkLoc;
                }
            }
        }
        internal Vector2 positionWithOrigin
        {
            get { return Position - Origin; }
        }
        public Vector2 LocalPosition
        {
            get { return _Position; }
        }
        public Vector2 SetPosition(Vector2 newPos)
        {
            Position = newPos;
            return newPos;
        }
        public Vector2 DrawPosition
        {
            get
            {
                return (positionWithOrigin) - game.camera.position.GetPoint();
            }
        }
        
        public Rectangle Hitbox = new Rectangle(0, 0, 0, 0);
        public Size SpriteSize 
        { get { return new Size(Width, Height); } }
        public Rectangle Bounds
        {
            get
            {
                Point l = (Position - _Origin).GetPoint();
                return new Rectangle(l.X + Hitbox.X, l.Y + Hitbox.Y, (int)(Hitbox.Size.Width * Scale.X), (int)(Hitbox.Size.Height * Scale.Y)); 
            }
        }
        public Rectangle SpriteBounds
        {
            get
            {
                Point l = (Position).GetPoint();
                if (Sprite == null) return new Rectangle(l.X, l.Y, (int)(Hitbox.Width * Scale.X), (int)(Hitbox.Height * Scale.Y));
                return new Rectangle(l.X, l.Y, (int)(Sprite.Image.Width * Scale.X), (int)(Sprite.Image.Height * Scale.Y));
            }
        }

        public GameObject Parent
        {
            get;
            set;
        }

        private Vector2 _Origin
        {
            get 
            { 
                if(Sprite == null)
                    return new Vector2(0, 0); 
                else
                    return new Vector2((Sprite.Base_image.Width * Scale.X) / 2, (Sprite.Base_image.Height * Scale.Y) / 2); 
            }
        }
        public Vector2 Origin
        {
            get 
            { 
                if(Sprite == null)
                    return new Vector2(0, 0); 
                else
                    return new Vector2((Sprite.Image.Width * Scale.X) / 2, (Sprite.Image.Height * Scale.Y) / 2); 
            }
        }

        private Color _Tint = Color.Empty;
        public Color Tint
        {
            get { return _Tint; }
            set { _Tint = value; Sprite.Tint(value); }
        }

        private Vector2 _Scale = Vector2.One;
        public Vector2 Scale 
        { get { return _Scale; } set { _Scale = value; } }

        private float _Rotation;
        public float Rotation
        {
            get
            {
                return _Rotation;
            }
            set { _Rotation = value; }
        }

        public bool _Enabled = true;
        public bool Enabled
        {
            get
            {
                return _Enabled;
            }
            set { _Enabled = value; }
        }
        public bool EnableCollisionDetection = true;
        public bool IsTrigger = false;

        public int Width 
        { get { return (int)(Sprite.Image.Width * Scale.X); } }
        public int Height 
        { get { return (int)(Sprite.Image.Height * Scale.Y); } }

        public GameObject(Sprite sprite)
        {
            this.Sprite = sprite;
        }
        public GameObject(Sprite sprite, Vector2 position)
        {
            this.Sprite = sprite;
            this.Position = position;
        }

        internal void Update(float deltaTime)
        {
            if (!Enabled)
                return;

            if (Sprite != null) Sprite.Update(deltaTime);

            Old_Position = new Vector2(Position.X, Position.Y);

            OnUpdate(deltaTime);

            if (Position != Old_Position)
            {
                if (OnMoveEvent != null) OnMoveEvent.Invoke(this, Old_Position);
                _OnMove();
            }
        }
        internal void Render(Graphics g)
        {
            if (Enabled)
            {
                if (Sprite != null)
                { 
                    if (Rotation != Sprite.rotation)
                    {
                        if (OnRotateEvent != null) OnRotateEvent.Invoke(this, Sprite.rotation);

                        OnRotate();
                        Sprite.Rotate(Rotation);
                        //Sprite.Tint(Tint);
                    }
                }
                OnRender(g);
            }
        }
        internal void _OnMove()
        {
            OnMove();
        }

        public virtual void OnAdded()
        {

        }
        public virtual void OnUpdate(float deltaTime)
        {

        }
        public virtual void OnRender(Graphics g)
        {
            Rendering.Render.RenderGameObject(game, g, this);
        }
        public virtual void OnCollision(GameObject go, Vector2 velocity)
        {
        }
        public virtual void OnMove() { }
        public virtual void OnDestroy() { }
        public virtual void OnRemove() { }
        public virtual void OnRotate() { }

        public void Destroy()
        {
            OnDestroy();
            game.scene.RemoveObject(this);
        }

        public void Move(Vector2 velocity)
        {
            Vector2 newPos = Vector2.Empty;
            if (EnableCollisionDetection && game.UseInbuiltCollisionDetection)
            {
                // X
                GameObject goX = game.scene.isBlocked(this, new Rectangle((int)(Bounds.X + velocity.X), (int)Bounds.Y, Bounds.Width, Bounds.Height), velocity);
                if (goX != null)
                {
                    Rectangle boundsX = new Rectangle((int)goX.Bounds.X, (int)goX.Bounds.Y, goX.Bounds.Width, goX.Bounds.Height);
                    // Right
                    if (velocity.X > 0)
                        newPos.X = boundsX.Left - (Bounds.Width / 2);
                    // Left
                    if (velocity.X < 0)
                        newPos.X = boundsX.Right + (Bounds.Width / 2);

                    SetPosition(new Vector2(newPos.X, LocalPosition.Y));
                    velocity.X = 0;
                }

                // Y
                GameObject goY = game.scene.isBlocked(this, new Rectangle((int)Bounds.X, (int)(Bounds.Y + velocity.Y), Bounds.Width, Bounds.Height), velocity);
                if (goY != null)
                {
                    Rectangle boundsY = new Rectangle((int)goY.Bounds.X, (int)goY.Bounds.Y, goY.Bounds.Width, goY.Bounds.Height);
                    // Up
                    if (velocity.Y < 0)
                        newPos.Y = boundsY.Bottom + (Bounds.Width / 2);
                    // Down
                    if (velocity.Y > 0)
                        newPos.Y = boundsY.Top - (Bounds.Width / 2);

                    SetPosition(new Vector2(LocalPosition.X, newPos.Y));
                    velocity.Y = 0;
                }

            }

            SetPosition(new Vector2(velocity.X + LocalPosition.X, velocity.Y + LocalPosition.Y));
        }

        public T Clone<T>() where T : GameObject
        {
            return (T)this.MemberwiseClone();
        }

        // Methods
        public Vector2 Forward
        {
            get
            {
                float angle = Rotation * ((float)Math.PI / 180);
                return new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
            }
        }

        private void _LookAt(Vector2 pos, float min = -1, float max = -1)
        {
            float angle = (float)Math.Atan2(pos.Y - Position.Y, pos.X - Position.X) * (float)(180 / Math.PI);

            if(min != -1 || max != -1)
                angle = Helper.Helper.Clamp<float>(angle, min, max);

            Rotation = angle;
        }
        public void LookAt(Vector2 pos) { _LookAt(pos); }
        public void LookAt(Vector2 pos, float min, float max) { _LookAt(pos, min, max); }
        public void LookAt(GameObject go) { _LookAt(go.Position); }
        public void LookAtMouse()
        {
            _LookAt(game.camera.ScreenToWorld(Mouse.MousePosition));
        }

        // Methods
        public void MoveTowardsMouse(float Speed, float DeltaTime)
        {
            _MoveTowards(game.camera.ScreenToWorld(Mouse.MousePosition), Speed, DeltaTime);
        }
        public void MoveForward(float Speed, float DeltaTime)
        {
            Move(Forward * Speed * DeltaTime);
        }
        private void _MoveTowards(Vector2 _Position, float Speed, float DeltaTime)
        {
            float angle = (float)Math.Atan2(_Position.Y - Position.Y, _Position.X - Position.X);
            Vector2 velocity = Vector2.Empty;

            velocity.X = (float)Math.Cos(angle) * Speed * DeltaTime;
            velocity.Y = (float)Math.Sin(angle) * Speed * DeltaTime;

            Move(velocity);
        }
        public void MoveTowards(Vector2 Position, float Speed, float DeltaTime) { _MoveTowards(Position, Speed, DeltaTime); }
        public void MoveTowards(GameObject gameObject, float Speed, float DeltaTime) { _MoveTowards(gameObject.Position, Speed, DeltaTime); }

        private void _MoveAway(Vector2 _Position, float Speed, float DeltaTime)
        {
            float angle = (float)Math.Atan2(Position.Y - _Position.Y, Position.X - _Position.X);
            Vector2 velocity = Vector2.Empty;

            velocity.X = (float)Math.Cos(angle) * Speed * DeltaTime;
            velocity.Y = (float)Math.Sin(angle) * Speed * DeltaTime;

            Move(velocity);
        }
        public void MoveAway(Vector2 Position, float Speed, float DeltaTime) { _MoveAway(Position, Speed, DeltaTime); }
        public void MoveAway(GameObject gameObject, float Speed, float DeltaTime) { _MoveAway(gameObject.Position, Speed, DeltaTime); }

        public void Resize(int size)
        {
            Sprite._Resize(size);
            Hitbox.Width *= size;
            Hitbox.Height *= size;
        }

        public void Dispose()
        {
            Sprite.Dispose();
        }
    }
}
